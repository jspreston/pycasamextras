# Debugging, just add DebugHere() to start debugger at that line in
# ipython
try:
    from IPython.core.debugger import Tracer
    DebugHere = Tracer()
except ImportError:
    def DebugHere():
        print 'DebugHere encountered when not in ipython'
        #raise Exception('DebugHere encountered when not in ipython')
