import PyCA.Core as core
import PyCA.Common as common

import numpy as np

import Param

import os

try:
    import PIL
    HAVE_PIL=True
except:
    HAVE_PIL=False

FrameDataParam = \
    Param.ParentParam(
    name='FrameData',
    children=[Param.Param(name='DataSet', 
                          value='',
                          comment='volumetric file to load frames from'),
              Param.Param(name='FrameList', 
                          value=[0,1,2],
                          comment='list of frames to load'),
              Param.Param(name='DSFac', 
                          value=1,
                          comment='downsampling factor'),
              Param.Param(name='NiceDS',
                          value=False,
                          comment='do nice downsampling using PIL?'),
              Param.Param(name='Rescale', 
                          value=True,
                          comment='rescale to range [0,1]'),
              Param.Param(name='CropStart', 
                          value=[0,0],
                          comment='[xmin, ymin]'),
              Param.Param(name='CropSz', 
                          value=None,
                          comment='[xsz, ysz] or None')],
    comment='load frames from volume')

#
# Load a series of (possibly non-consecutive) frames from a 3D volume
# of data, and perform appropriate downsampling and scaling, and
# possible zero-border and tukey window fade
#
def LoadFrameData(fname, frameList, 
                  mType=core.MEM_HOST, DSFac=1,
                  useNiceDS=False,
                  rescale=True,
                  cropStart=None, cropSz=None,
                  outSz=None,
                  borderPix=0, tWinPix=0):
    """
    Load z slices listed in framelist from 'fname', returning a list of
    Image3Ds appropriately downsampled and cropped, and of the
    requested memory type, and with any requested masking applied.
    cropSz specifies the final output size, after downsampling.  if
    cropSz is specified without cropStart, crop center of image.  If
    outSz is specified, do a final resampling to this size.
    """

    fname = os.path.expandvars(fname)
    dataIm = common.LoadImage(fname, mType=core.MEM_HOST)

    fullSz = dataIm.size().tolist()
    nFrames = len(frameList)
    
    if cropStart is None:
        if cropSz is None:
            cropStart=[0,0]
        else:
            cropStart=(np.array(fullSz[:2])//2-np.array(cropSz)//2).tolist()

    assert cropStart[0] >= 0
    assert cropStart[1] >= 0
    assert cropStart[0] < fullSz[0]
    assert cropStart[1] < fullSz[1]
    
    if cropSz is None:
        cropEnd = fullSz
    else:
        cropEnd = np.array(cropStart) + np.array(cropSz)*DSFac
        assert cropEnd[0] > 0
        assert cropEnd[1] > 0
        assert cropEnd[0] <= fullSz[0]
        assert cropEnd[1] <= fullSz[1]
        
    # extract a series of non-consecutive frames
    cropSz = np.array([cropEnd[0]-cropStart[0],
                       cropEnd[1]-cropStart[1]])
    volArr = np.zeros((cropSz[0],
                       cropSz[1],
                       len(frameList)))
    for frameIdx in range(nFrames):
        frame = frameList[frameIdx]
        extractExtent = [cropStart[0],cropEnd[0]-1,
                         cropStart[1],cropEnd[1]-1,
                         frame,frame]
        sliceIm = common.ExtractROI(dataIm,extractExtent)
        volArr[:,:,frameIdx] = np.squeeze(sliceIm.asnp())
    
    # downsample data in xy plane
    if outSz is None:
        outSz = cropSz//DSFac
    elif not useNiceDS:
        raise Exception('specifying outSz requires useNiceDS')
    else:
        outSz = np.array(outSz)

    if useNiceDS:
        if not HAVE_PIL:
            raise Exception('NiceDS requested, but PIL not available')
        newArr = np.zeros(np.append(outSz, nFrames))
        for zIdx in range(volArr.shape[2]):
            im = PIL.Image.fromarray(volArr[:,:,zIdx])
            im = im.resize([outSz[1], outSz[0]], PIL.Image.ANTIALIAS)
            newArr[:,:,zIdx] = np.array(im)
        volArr = newArr
    else:
        volArr = volArr[::DSFac,::DSFac,:]

    sz = np.array(list(volArr.shape))
    sliceSz = core.Vec3Di(int(sz[0]),int(sz[1]),1)
    grid = core.GridInfo(sliceSz)

    # rescale data
    if rescale:
        rng = core.MinMax(dataIm)
        volArr = (volArr-rng[0])/(rng[1]-rng[0])

    # border mask
    TWin = common.TukeyWindow(sz[0:2]-2*borderPix,tWinPix)
    mask = np.zeros(sz[0:2])
    mask[borderPix:sz[0]-borderPix,borderPix:sz[1]-borderPix] = TWin
    volArr = volArr*np.tile(np.atleast_3d(mask),(1,1,sz[2]))
    volArr = volArr+(1.0-np.tile(np.atleast_3d(mask),(1,1,sz[2])))

    I = []
    for idx in range(nFrames):
        im = common.ImFromNPArr(volArr[:,:,idx], mType=mType,
                                sp=dataIm.spacing(),
                                orig=dataIm.origin())
        I.append(im)
    return I

def LoadFrameDataFromParam(param, mType=core.MEM_HOST):

    I = \
      LoadFrameData(fname=param.DataSet(),
                    frameList=param.FrameList(),
                    DSFac=param.DSFac(),
                    useNiceDS=param.NiceDS(),
                    rescale=param.Rescale(),
                    cropStart=param.CropStart(),
                    cropSz=param.CropSz())
                  
    return I
