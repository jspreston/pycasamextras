import PyCA.Core as ca
import PyCA.Common as common

import numpy as np

class PyCAMat3D(object):
    
    def __init__(self,
                 grid=None,
                 mType=None,
                 rows=None,
                 copyData=False):
        """
        Either specify grid (for empty matrix) or list of three vectors
        making up the rows of the matrix.
        """
        
        if rows is not None:
            self.init_from_rows(rows, grid=grid, mType=mType, copyData=copyData)
        elif grid is not None:
            self.init_empty(grid, mType)
        else:
            raise Exception('Error, must specify either grid or rows')

    def init_from_rows(self, rows, grid=None, mType=None, copyData=False):
        assert len(rows) == 3
        for r in range(3):
            assert common.IsField3D(rows[r])
            if grid is not None:
                assert grid == rows[r].grid()
            if mType is not None:
                assert mType == rows[r].memType()
        self.grid = rows[0].grid().copy()
        self.mType = rows[0].memType()
        for r in range(3):
            assert self.grid == rows[r].grid()
            assert self.mType == rows[r].memType()
        if copyData:
            rows = [r.copy() for r in rows]
        self.r0 = rows[0]
        self.r1 = rows[1]
        self.r2 = rows[2]

    def init_empty(self, grid, mType=None):
        if mType is None:
            mType = ca.MEM_HOST
        self.mType = mType
        self.grid = grid.copy()
        self.r0 = ca.Field3D(self.grid, self.mType)
        self.r1 = ca.Field3D(self.grid, self.mType)
        self.r2 = ca.Field3D(self.grid, self.mType)

    def row(self, idx):
        if idx == 0:
            return self.r0
        elif idx == 1:
            return self.r1
        elif idx == 2:
            return self.r2
        else:
            raise IndexError('index out of range')

    def copy(self):
        return PyCAMat3D(rows=(self.r0, self.r1, self.r2),
                         copyData=True)

    def equals(self, other, eps=0.0, verbose=False):
        assert type(other) is PyCAMat3D

        diff = 0.0
        tmpF = ca.Field3D(self.grid, self.mType)
        tmpI = ca.Image3D(self.grid, self.mType)
        for r in range(3):
            ca.Sub(tmpF, self.row(r), other.row(r))
            ca.Magnitude(tmpI, tmpF)
            ca.Sqr_I(tmpI)
            diff += ca.Sum(tmpI)
        diff = np.sqrt(diff)/self.grid.nVox()

        if verbose:
            print 'equality diff: %f'%diff
        
        if diff > eps:
            return False

        return True
        
    def nVox(self):
        return self.grid.nVox()
                
    def RightVecMul(self, vf):
        """
        Right-multiply matrix by vf (interpreted as column vectors),
        return resulting vector field
        """
        result = ca.Field3D(self.grid, self.mType)
        tmp = ca.Image3D(self.grid, self.mType)
        ca.ComponentDotProd(tmp, self.r0, vf)
        ca.Copy(result, tmp, 0)
        ca.ComponentDotProd(tmp, self.r1, vf)
        ca.Copy(result, tmp, 1)
        ca.ComponentDotProd(tmp, self.r2, vf)
        ca.Copy(result, tmp, 2)
        return result

    def LeftVecMulT(self, vf):
        """
        Left-multiply matrix by vf (interpreted as row vectors),
        return resulting vector field
        """
        out = ca.Field3D(self.grid, self.mType)
        tmpM = ca.Image3D(self.grid, self.mType)
        tmpV = ca.Image3D(self.grid, self.mType)
        tmpOut = ca.Image3D(self.grid, self.mType)
        for c in range(3):
            ca.SetMem(tmpOut, 0.0)
            for r in range(3):
                ca.Copy(tmpM, self.row(r), c)
                ca.Copy(tmpV, vf, r)
                ca.Add_Mul_I(tmpOut, tmpM, tmpV)
            ca.Copy(out, tmpOut, c)
        return out
    
    def LeftMatMul(self, M):
        """self = M*self"""
        self.MatMul(M, right=False)

    def RightMatMul(self, M):
        """self = self*M"""
        self.MatMul(M, right=True)
        
    def MatMul(self, M, right=True):

        rows = [ca.Field3D(self.grid, self.mType) for _ in range(3)]
        for r in rows:
            ca.SetMem(r, 0.0)

        tmpL = ca.Image3D(self.grid, self.mType)
        tmpR = ca.Image3D(self.grid, self.mType)
        tmpOut = ca.Image3D(self.grid, self.mType)

        if right:
            L = self
            R = M
        else:
            L = M
            R = self
            
        for r in range(3):
            for c in range(3):
                ca.SetMem(tmpOut, 0.0)
                for dp in range(3):
                    ca.Copy(tmpL, L.row(r), dp)
                    ca.Copy(tmpR, R.row(dp), c)
                    ca.Add_Mul_I(tmpOut, tmpL, tmpR)
                ca.Copy(rows[r], tmpOut, c)
            
        self.init_from_rows(rows)
        
        
    def Transpose(self, swap=False):
        """
        Transpose this matrix in-place
        """
        if swap:
            self.r0.mDataY.swap(self.r1.mDataX)
            self.r0.mDataZ.swap(self.r2.mDataX)
            self.r1.mDataZ.swap(self.r2.mDataY)
        else:
            tmpA = ca.Image3D(self.grid, self.mType)
            tmpB = ca.Image3D(self.grid, self.mType)
            # swap M[0][1] and M[1][0]
            ca.Copy(tmpA, self.r0, 1)
            ca.Copy(tmpB, self.r1, 0)
            ca.Copy(self.r0, tmpB, 1)
            ca.Copy(self.r1, tmpA, 0)
            # swap M[0][2] and M[2][0]
            ca.Copy(tmpA, self.r0, 2)
            ca.Copy(tmpB, self.r2, 0)
            ca.Copy(self.r0, tmpB, 2)
            ca.Copy(self.r2, tmpA, 0)
            # swap M[1][2] and M[2][1]
            ca.Copy(tmpA, self.r1, 2)
            ca.Copy(tmpB, self.r2, 1)
            ca.Copy(self.r1, tmpB, 2)
            ca.Copy(self.r2, tmpA, 1)

    def SetMemPos(self, val, pos):
        """
        Set an entry of each 3x3 matrix in the field.  `pos` should be
        the 2d coord (row,col) of the matrix entry.
        """
        if type(val) is int or type(val) is float:
            raise Exception('scalar val expected')
        assert len(pos) == 2
        for p in pos:
            assert 0 <= p < 3

        tmp = ca.Image3D(self.grid, self.mType)
        ca.SetMem(tmp, val)
        ca.Copy(self.row(pos[0]), tmp, pos[1])
        
            
    def SetMem(self, val):
        """
        Initialize matrix value(s) from scalar or 3x3 numpy matrix.
        """

        if type(val) is int or type(val) is float:
            val = val*np.ones((3,3))
        elif type(val) is np.ndarray:
            assert val.shape == (3,3)
        else:
            raise Exception('Unknown type for `val`')

        for r in range(3):
            for c in range(3):
                self.SetMemPos(val[r,c], (r,c))

    def eye(self):
        self.SetMem(np.eye(3))

    def Compose(self, h):
        tmp = ca.Field3D(self.grid, self.mType)
        ca.ApplyH(tmp, self.r0, h, ca.BACKGROUND_STRATEGY_CLAMP)
        ca.Copy(self.r0, tmp)
        ca.ApplyH(tmp, self.r1, h, ca.BACKGROUND_STRATEGY_CLAMP)
        ca.Copy(self.r1, tmp)
        ca.ApplyH(tmp, self.r2, h, ca.BACKGROUND_STRATEGY_CLAMP)
        ca.Copy(self.r2, tmp)

    def tonp(self):
        MatArr = np.concatenate([common.AsNPCopy(r)[:,:,:,:,None] for r in (self.r0, self.r1, self.r2)], axis=4)
        MatArr = np.transpose(MatArr, (0,1,2,4,3))
        return MatArr

    def fromnp(self, MatArr):
        rows = [common.FieldFromNPArr(np.squeeze(MatArr[:,:,:,[r],:],axis=3)) for r in range(3)]
        self.init_from_rows(self, rows)


def Jacobian(vf):
    grid = vf.grid().copy()
    mType = vf.memType()
    
    r0 = ca.Field3D(grid, mType)
    r1 = ca.Field3D(grid, mType)
    r2 = ca.Field3D(grid, mType)
    
    ca.Jacobian(r0, r1, r2, vf)

    mat = PyCAMat3D(rows=(r0,r1,r2))

    return mat
#
# numpy matrix fields for testing
#
def is_valid_mat(M):
    if len(M.shape) != 5:
        return False
    if np.any(np.array(M.shape[3:]) != np.array([3,3])):
        return False
    return True

def is_valid_vec(v):
    if len(v.shape) != 4:
        return False
    if v.shape[3] != 3:
        return False
    return True

def base_sz(M_or_v):
    return np.array(M_or_v.shape[:3])

def NPMatTranspose(M):
    assert(is_valid_mat(M))
    return np.transpose(M, (0,1,2,4,3))
    
def NPMatVecRMul(M, v):
    assert(is_valid_mat(M))
    assert(is_valid_vec(v))
    sz = base_sz(M)
    assert np.all(sz == base_sz(v))
    out = np.zeros(v.shape)
    for r in range(3):
        for c in range(3):
            out[:,:,:,r] += M[:,:,:,r,c]*v[:,:,:,c]
    return out

def NPMatVecLMulT(v, M):
    assert(is_valid_mat(M))
    assert(is_valid_vec(v))
    sz = base_sz(M)
    assert np.all(sz == base_sz(v))
    out = np.zeros(v.shape)
    for c in range(3):
        for r in range(3):
            out[:,:,:,c] += v[:,:,:,r]*M[:,:,:,r,c]
    return out

def NPMatMul(L,R):
    assert(is_valid_mat(R))
    assert(is_valid_mat(L))
    assert np.all(base_sz(L) == base_sz(R))

    LR = np.zeros(R.shape)

    for r in range(3):
        for c in range(3):
            for dp in range(3):
                LR[:,:,:,r,c] += L[:,:,:,r,dp]*R[:,:,:,dp,c]
    return LR


def TestMatMul(M0, M1):
    M0Arr = M0.tonp()
    M1Arr = M1.tonp()

    RMul = M0.copy()
    RMul.RightMatMul(M1)
    RMulArr = RMul.tonp()

    RMulArr2 = NPMatMul(M0Arr, M1Arr)

    RDiff2 = (RMulArr-RMulArr2)**2
    diff = np.sqrt(np.sum(RDiff2))/M0.nVox()

    print 'RMul diff = %f'%diff

    LMul = M0.copy()
    LMul.LeftMatMul(M1)
    LMulArr = LMul.tonp()

    LMulArr2 = NPMatMul(M1Arr, M0Arr)

    LDiff2 = (LMulArr-LMulArr2)**2
    diff = np.sqrt(np.sum(LDiff2))/M0.nVox()

    print 'LMul diff = %f'%diff

    plt.figure('mat diff')
    plt.clf()

    plt.subplot(121)
    plt.imshow(LDiff2[:,:,32,0,0])
    plt.colorbar()
    plt.draw()
    plt.subplot(122)
    plt.imshow(RDiff2[:,:,32,0,0])
    plt.colorbar()
    plt.draw()

    plt.show()

if __name__ == '__main__':

    import matplotlib.pyplot as plt
    import PyCA.Display as display

    mType = ca.MEM_DEVICE
    sz = [64,64,64]

    np.random.seed(17)
    randField = common.RandField(sz=sz, nSig=1.0, gSig=0.0, mType=mType)
    M = Jacobian(randField)

    # test transpose
    MArr = M.tonp()
    
    MTest = M.copy()
    MTest.Transpose()
    MArrT = MTest.tonp()
    MTest.Transpose()
    if M.equals(MTest, eps=1e-8, verbose=True):
        print 'double transpose matches original'
    else:
        print 'Error: double transpose fails'
        
    MArrT2 = NPMatTranspose(MArr)
    diff = np.sqrt(np.sum((MArrT-MArrT2)**2))/M.nVox()
    print 'numpy transpose diff: ', diff
    
    # test LeftMulT vs. Transpose + RightMul
    v = common.RandField(sz=sz, nSig=1.0, gSig=0.0, mType=mType)
    vArr = common.AsNPCopy(v)
    resultA = M.LeftVecMulT(v)
    M.Transpose(swap=False)
    resultB = M.RightVecMul(v)
    vDiff = resultA-resultB
    rng = ca.MinMax(vDiff)
    print rng

    # test numpy vs. pyca matrix multiplication
    plt.figure('np/pyca matmul diff')
    plt.clf()
    
    plt.subplot(121)
    v1 = NPMatVecLMulT(vArr, MArr)
    v2 = common.AsNPCopy(resultA)
    diffIm = np.squeeze(np.sum((v1-v2)**2,axis=3))
    plt.imshow(diffIm[:,:,32])
    plt.colorbar()
    diff = np.sqrt(np.sum(diffIm))/v.nVox()
    print 'numpy lmulT diff: ', diff
    plt.draw()

    plt.subplot(122)
    v1 =  NPMatVecRMul(MArrT, vArr)
    v2 = common.AsNPCopy(resultB)
    diffIm = np.squeeze(np.sum((v1-v2)**2,axis=3))
    plt.imshow(diffIm[:,:,32])
    plt.colorbar()
    diff = np.sqrt(np.sum(diffIm))/v.nVox()
    print 'numpy rmul diff: ', diff
    plt.draw()

    plt.show()

    

    plt.figure('results')
    plt.clf()
    plt.subplot(131)
    comp = ca.Image3D(resultA.grid(), resultA.memType())
    ca.Copy(comp, resultA, 0)
    display.DispImage(comp, 'resultA', newFig=False)
    plt.colorbar()
    plt.subplot(132)
    ca.Copy(comp, resultB, 0)
    display.DispImage(comp, 'resultB', newFig=False)
    plt.colorbar()
    plt.subplot(133)
    ca.Copy(comp, vDiff, 0)
    display.DispImage(comp, 'diff', newFig=False)
    plt.colorbar()
    plt.show()

    randField2 = common.RandField(sz=sz, nSig=1.0, gSig=0.0, mType=mType)
    M2 = Jacobian(randField2)

    TestMatMul(M, M2)
