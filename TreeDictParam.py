"""
Working on a param based on TreeDictParam, not working yet
"""

from treedict import TreeDict
import Enum

import copy

ParamImportance = Enum.Enum('REQUIRED','COMMON','RARE','DEBUG')

# default may be needed separate from None value
class DefaultType:
    pass

DEFAULT = DefaultType()

class ParamDTDBase(object):

    def __init__(self,
                 name,
                 importance=ParamImportance.COMMON,
                 comment=None):

        assert type(name) is str
        self.name = name
        self.importance = importance
        if comment is not None:
            assert type(comment) is str
        self.comment = comment

    def required(self):
        """Returns True if parameter is required to be set by user"""
        return self.importance == ParamImportance.REQUIRED

    def description(self, multiline=True):
        """Generate string description of param from comment"""
        s = ''
        if self.required():
            s += 'REQUIRED '
        if self.comment is not None:
            s += self.comment
        return s

    def indent(self, level=0):
        return '    '*level;

    def yamlComment(self, comment, level=0):
        return \
            ''.join([self.indent(level)+'# '+line+'\n'
                     for line in comment.strip().split('\n')])

    def __str__(self):
        return self.toYAML(multiline=False)

# end class ParamDTDBase

class ParamDTDValue(ParamDTDBase):

    def __init__(self,
                 name,
                 default=None,
                 importance=ParamImportance.COMMON,
                 datatype=None,
                 comment=None):

        super(ParamDTDValue, self).__init__(name=name,
                                            importance=importance,
                                            comment=comment)

        self.default = default
        if datatype is not None:
            assert type(datatype) is type
            if default is not None:
                assert type(default) is datatype
        self.datatype = datatype

    def validate(self, param, fill=False, minimal=False):

        if param is DEFAULT:
            if self.required():
                raise ParamParseError('Required param %s not defined' % \
                                      self.name)
            else:
                if fill and not minimal:
                    return copy.deepcopy(self.default)
                else:
                    return DEFAULT
        else:
            # ensure correct datatype
            if self.datatype is not None:
                if type(param) != self.datatype:
                    raise Exception('incorrect datatype: %r, should be %r'%(type(param), self.datatype))
            return param

    def toYAML(self, param, multiline=True, level=0):
        """Output as YAML text"""
        s = ''
        desc = self.description(multiline=multiline)

        if multiline:
            if len(desc):
                s += self.yamlComment(comment=desc, level=level)
            # the way to write None is a missing key in a dict, so
            # comment out the key
            s += self.indent(level)
            if param == None:
                s += '# '
            s += '%s: %s'%(self.name, repr(param))
        else:
            # the way to write None is a missing key in a dict, so
            # comment out the key
            if self.value == None:
                s += '# '
            s += '%s: %s'%(self.name, repr(param))
            if len(desc):
                s += '  # ' + desc
        return s

# end class ParamDTDValue

class ParamDTDParent(ParamDTDBase):
    """
    parameter definition for parameter with named children
    """

    def __init__(self,
                 name,
                 children=[],
                 importance=ParamImportance.COMMON,
                 comment=None):

        super(ParamDTDParent, self).__init__(name=name,
                                          importance=importance,
                                          comment=comment)

        self.children = dict([(c.name, c) for c in children])

    def validate(self, ob, fill=False, minimal=False):

        if ob is not DEFAULT and type(ob) is not dict:
            raise ObParseError(self.name + ': ParentOb should only encounter dict object')

        if ob is DEFAULT:
            if self.required():
                raise ObParseError('Required ob %s not defined' % \
                                      self.name)
            else:
                if not fill or minimal:
                    return DEFAULT
                else:
                    ob = {}

        param = TreeDict(self.name)

        if fill:
            for name in self.children.keys():
                if name not in ob.keys():
                    ob[name] = DEFAULT
        else:
            ob_names = ob.keys()

        for name in self.children.keys():
            if fill or name in ob_names:
                param.__setattr__(name,
                                  self.children[name].validate(ob[name], fill=fill, minimal=minimal))
        return param

    def toYAML(self, param, multiline=True, level=0):
        """Output as YAML example"""
        s = ''
        desc = self.description(multiline=multiline)
        if multiline:
            if len(desc):
                s += self.yamlComment(comment=desc, level=level)
            s += self.indent(level) + self.name + ':\n'
        else:
            s += self.indent(level) + self.name + ':'
            if len(desc):
                ' #' + desc
            s += '\n'

        for key, val in param.iteritems(recursive=False, branch_mode='all'):
            child = self.children[key]
            s += child.toYAML(param=val, multiline=multiline, level=level+1) + '\n'

        return s

    def __getattr__(self, name):
        return self.children[name]

# end class ParamDTDParent

class ParamDTDList(ParamDTDBase):
    """
    Parameter class that parses a list of the same type of child params
    """

    def __init__(self,
                 name,
                 template,
                 importance=ParamImportance.COMMON,
                 comment=None,
                 updateDefaultOnParse=True):

        super(ParamDTDList, self).__init__(name=name,
                                           importance=importance,
                                           comment=comment)
        self.updateDefaultOnParse = updateDefaultOnParse

        # make sure template is valid
        if not isinstance(template, ParamDTDBase):
            raise ParamConstructError('ParamDTDList recieved non-ParamDTD '
                                      'template')
        self.template = template

    def validate(self, ob, fill=False, minimal=False):

        if param is not DEFAULT and type(param) is not list:
            raise ParamParseError(self.name + ': ParamDTDList should only encounter list objects')

        if param is DEFAULT:
            if self.required():
                raise ParamParseError('Required param %s not defined' % \
                                      self.name)
            else:
                if not fill or minimal:
                    return DEFAULT

        param = []
        for obitem in ob:
            paramitem = template.validate(obitem, fill=fill, minimal=minimal)
            param.append(paramitem)
        return param

    def toYAML(self, param, multiline=True, level=0):
        """Output as YAML example"""
        desc = self.description(multiline=multiline)
        s = ''
        if multiline:
            if len(desc):
                s += self.yamlComment(comment=desc, level=level)
            s += self.indent(level) + self.name + ':\n'
        else:
            s += self.indent(level) + self.name + ':'
            if len(desc):
                ' #' + desc
            s += '\n'
        for p in param:
            s += self.indent(level) + '-\n'
            s += template.toYAML(param=p, multiline=multiline, level=level+1) + '\n'
        return s

# end class ListParam

if __name__ == '__main__':

    # test
    paramDTD = \
    ParamDTDParent(name='testparam',
                   children=[
                       ParamDTDValue(name='l1val',
                                     default='l1defaultval',
                                     importance=ParamImportance.COMMON,
                                     datatype=None,
                                     comment='l1val parameter'),
                       ParamDTDValue(name='l1val2',
                                     default='l1defaultval2',
                                     importance=ParamImportance.COMMON,
                                     datatype=None,
                                     comment='l1val2 parameter'),
                       ParamDTDParent(name='l1pp',
                                      children=[
                                          ParamDTDValue(name='l2val',
                                                        default='l2defaultval',
                                                        importance=ParamImportance.COMMON,
                                                        datatype=None,
                                                        comment='l2val parameter'),
                                          ParamDTDValue(name='l2otherparam',
                                                        default='l2otherval',
                                                        importance=ParamImportance.COMMON,
                                                        datatype=None,
                                                        comment='l2val other parameter')

                                          ],
                                      importance=ParamImportance.COMMON,
                                      comment='parent param')
                       ],
                   importance=ParamImportance.COMMON,
                   comment='top level param')

    param = TreeDict()
    paramDTD.validate(param.convertTo('nested_dict'), fill=True)
    print paramDTD.toYAML(param)
