import copy

"""
Simple enum type, can be used as:

>>> MyEnum = Enum.Enum('FOO', 'BAR')
>>> MyEnum.FOO
0
>>> MyEnum.BAR
1

Or you can assign values:

>>> MyEnum = Enum.Enum(FOO=5, BAR=6)
>>> MyEnum.FOO
5
>>> MyEnum.BAR
6

You can also mix and match, but there is no guarantee that there will
not be duplicate assignments:

>>> MyEnum = Enum.Enum('FOO', 'BAR', BLAH=1)
>>> MyEnum.FOO
0
>>> MyEnum.BAR
1
>>> MyEnum.BLAH
1
"""

def Enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    obdict = copy.copy(enums)
    obdict['enum_mapping'] = enums
    reverse = dict((value, key) for key, value in enums.iteritems())
    obdict['reverse_mapping'] = reverse
    return type('Enum', (), obdict)
