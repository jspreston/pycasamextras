try:
    from mpi4py import MPI
    MPI_FOUND = True
except:
    MPI_FOUND = False
    print 'warning, unable to import mpi4py'

try:
    import PyCA.Core as ca
    import PyCA.Common as common
except ImportError:
    print 'warning, unable to import PyCA'

## try importing matplotlib, set backend to non-graphical if
## needed
import socket
HOSTNAME = socket.gethostname()
import re

# define the default root
ROOT_RANK = 0

try:
    # if we're on a tesla node, use the agg module
    if re.match('^tesla.*',HOSTNAME) is not None or \
           re.match('^kepler.*',HOSTNAME) is not None:
        import matplotlib
        matplotlib.use('agg')
        GRAPHICAL_DISP = False
        print 'warning, will not use graphical display'
    else:
        GRAPHICAL_DISP = True
        
    import matplotlib.pyplot as plt
    plt.ion()
    PLT_FOUND = True
except:
    PLT_FOUND = False
    GRAPHICAL_DISP = False
    print 'warning, unable to import matplotlib.pyplot'

import os, sys
import numpy as np
import threading

MPI_DEBUG=False

def SetDebug(dbg):
    MPI_DEBUG=dbg

def MPISetup():
    if MPI_FOUND:
        size = MPI.COMM_WORLD.Get_size()
        rank = MPI.COMM_WORLD.Get_rank()
        name = MPI.Get_processor_name()
        if size == 1:
            local_rank = 0
        else:
            local_rank = int(os.environ.get('OMPI_COMM_WORLD_LOCAL_RANK', -1))
    else:
        size = 1
        rank = 0
        name = 'unknown'
        local_rank = 0

    host = socket.gethostname()
    
    # ca.SetCUDADevice(local_rank)
    thread_name_format = "MPI {rank:d}/{size:d}, {host}#{local_rank:d}"

    # set the thread name to something reasonable
    thread_name = \
        thread_name_format.format(rank=rank,size=size,
                                  host=host,local_rank=local_rank)
    threading.current_thread().name = thread_name

    sys.stdout.write(
        "Hello, World! I am process %d of %d with name: %s "
        "and local rank: %d.\n"
        % (rank, size, name, local_rank))
    

    return size, rank, host, local_rank


# define module variables
MPI_SIZE, MPI_RANK, MPI_HOST, MPI_LOCAL_RANK = MPISetup()

def Distribute(elements):
    if type(elements) is int:
        return common.Distribute(elements, MPI_SIZE, MPI_RANK)
    elif type(elements) is list:
        myEls = common.Distribute(len(elements), MPI_SIZE, MPI_RANK)
        return [elements[elIdx] for elIdx in myEls]
    else:
        raise Exception('unknown type for elements in Distribute: '
                        + str(elements))

def Spawn(nproc=1, hostList=None, exitOnCompletion=True):
    """
    Spawn mpiexec if not already done.  If this spawns subprocesses,
    it either exits or returns True based on exitOnCompletion
    parameter.  Returns False if already in an MPI-spawned process.
    """
    # detect if we've started this process in MPI yet or do that now
    if not 'OMPI_COMM_WORLD_LOCAL_RANK' in os.environ:
        if hostList is None:
            hostList = ['localhost']

        print "Spawning " + str(nproc) + " MPI processes..."
        # TODO: use mpi4py for spawning instead of mpiexec
        #comm = MPI.COMM_SELF.Spawn(sys.executable,
                                    #args=sys.argv, maxprocs=nproc)
        opt = ''
        # opt += '--mca plm_base_verbose 5 '
        # opt += '--mca plm_rsh_agent ssh '
        # opt += '--mca orte_rsh_agent ssh '
        # opt += '--mca btl_tcp_if_exclude lo,eth0'
        opt += '-x PYTHONPATH '
        CMD = "mpiexec " + opt + "-n " + str(nproc) + " --host \"" +\
              ','.join(hostList) + "\" " + 'python ' +' '.join(sys.argv)
        print(CMD)
        os.system(CMD)
        if exitOnCompletion:
            print 'spawning process exiting'
            sys.exit(0)
        else:
            print 'subprocesses complete'
            return True
    return False

def MPIAllreduce(nparr, op=MPI.SUM, debugMsg=''):
    """
    run Allreduce on nparr -- nparr will contain reduced values
    afterwards
    """
    if MPI_SIZE > 1:
        nparrin = np.zeros(nparr.shape)
        if MPI_DEBUG and len(debugMsg) > 0:
            print 'Proc %d/%d entering reduce - %s'%\
                (MPI_RANK+1,MPI_SIZE,debugMsg)
        MPI.COMM_WORLD.Allreduce(nparr, nparrin, op=op)
        if MPI_DEBUG and len(debugMsg) > 0:
            print 'Proc %d/%d exiting reduce - %s'%\
                (MPI_RANK+1,MPI_SIZE,debugMsg)
        nparr[:] = nparrin[:]
    else:
        if MPI_DEBUG and len(debugMsg) > 0:
            print 'Proc %d/%d skipping reduce - %s'%\
                (MPI_RANK+1,MPI_SIZE,debugMsg)

def reduce_inplace(nparr, all=False, op=MPI.SUM):
    if MPI_SIZE == 1:
        # don't do anything if we're the only process
        return nparr
    
    if all:
        MPI.COMM_WORLD.Allreduce(MPI.IN_PLACE,
                                 nparr,
                                 op=op)
    else:
        sndbuf = MPI.IN_PLACE if MPI_RANK == ROOT_RANK else nparr
        recvbuf = nparr if MPI_RANK == ROOT_RANK else None
        MPI.COMM_WORLD.Reduce(sndbuf,
                              recvbuf,
                              op=op,
                              root=ROOT_RANK)
        
    # nparr contents have changed in-place, so return is unnecessary
    # but included for convenience
    return nparr

