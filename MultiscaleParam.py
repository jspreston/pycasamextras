

class MultiscaleParam(object):
    def __init__(self, NScales=0):
        self.NScales=NScales
        self.CurScale=-1
        self.__ScaleParams = {}

    def AddScaleParam(self, name, val):
        # make sure val is either a scalar value or a subscriptable
        # variable of the correct length
        if hasattr(val, "__getitem__"):
            if len(val) != self.NScales:
                raise IndexError('Parameter ' + name + ' has wrong number ' + \
                                     'of entries, ' + str(self.NScales) + 'expected.')
        else:
            val = [val]*self.NScales

        # keep the list of values in __ScaleParams
        self.__ScaleParams[name] = val

        # add the member variable (attribute) and set appropriate value
        curval = None
        if self.CurScale >= 0:
            curval = val[self.CurScale]
        self.__setattr__(name, curval)
        
    def SetScale(self, scale):
        # make sure scale is in range
        if scale >= self.NScales:
            raise IndexError('scale ' + scale + ' out of range')

        # set current scale
        self.CurScale=scale

        # update current value params
        for name,val in self.__ScaleParams.items():
            self.__setattr__(name, val[self.CurScale])

    def __str__(self):
        out = 'Single Values:\n'
        for attr in dir(self):
            val = self.__getattribute__(attr)
            if not (callable(val) \
                        or attr.startswith('__') \
                        or attr.startswith('_MultiscaleParam__')):
                if not attr in self.__ScaleParams.keys():
                    out += attr + ': ' + str(val) + '\n'
        out += 'Current Scale Values:\n'
        for attr in self.__ScaleParams.keys():
            val = self.__getattribute__(attr)
            out += attr + ': ' + str(val) + '\n'
        out += 'All Scale Values:\n'
        for (attr,val) in self.__ScaleParams.items():
            out += attr + ': ' + str(val) + '\n'
        return out

#
# TESTING
#

import unittest

class MultiscaleParamTestCase(unittest.TestCase):
    def setUp(self):
        self.firstParamValues = [0.0, 0.1, 0.2]
        self.secondParamValue = 0.05
        self.param = MultiscaleParam(3)
        self.param.AddScaleParam('FirstParam', self.firstParamValues)
        self.param.AddScaleParam('SecondParam', self.secondParamValue)
        self.param.SetScale(0)

    def test_AddScaleParam(self):
        # test that we get an exception when we add a scale parameter
        # with the wrong number of values
        with self.assertRaises(IndexError):
            self.param.AddScaleParam('AnotherParam', [0.01])

    def test_NScales(self):
        self.assertEqual(self.param.NScales, 3)

    def test_ScaleParamValues(self):
        for scale in range(self.param.NScales):
            self.param.SetScale(scale)
            self.assertEqual(self.param.FirstParam, self.firstParamValues[scale])

    def test_ScaleParamStr(self):
        # just make sure printing doesn't throw exception
        print self.param

        
if __name__ == '__main__':

    unittest.main()
