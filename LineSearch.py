import math

# constants we'll need
PHI = (1 + math.sqrt(5)) / 2.0
RPHI = 1.0 / (1.0 + PHI)

# result flags
LS_OKAY = 0
LS_MAX_STEP = 1
LS_NO_STEP = 2

#
# Algorithm parameters
#

# maximum number of iterations for expansion/contraction
MAX_IT = 30

#
# sDir - the search direction
#
# step0 - the initial step size
#
# getEn - function takeing the search dir and a step size and returns
# the evaluation of the objective function
#
# eps - stopping criteria, return when energy change <= eps
#
# maxStep - optional max step size. maxStep=0 means no maximum.
#

import logging
logger = logging.getLogger(__name__)

def LineSearch(sDir, step0, getEn, getDist, eps=0.01, maxStep=0):

    if step0 == 0:
        logger.warning('----Error, zero stepsize----')
        return (0.0, LS_NO_STEP)

    # compute initial energy
    enAtMin = getEn(sDir, 0.0)
    en0 = enAtMin
    # current step sizes
    stepMin = 0
    stepMax = step0
    # current far-bound energy
    enAtMax = getEn(sDir, stepMax)

    logger.info('initial min/max en: %f/%f', enAtMin, enAtMax)

    # set up initial conditions so we have minimum bounded
    if enAtMax >= enAtMin:
        # contract stepsize until we get decrease in energy
        stepMid = stepMax
        # force first step
        enAtMid = enAtMax + 1.0
        it = 1
        while enAtMid >= enAtMin and it < MAX_IT:
            # if we've already increased energy then pick midpoint
            stepMid = RPHI * stepMid
            enAtMid = getEn(sDir, stepMid)
            it += 1
        if it >= MAX_IT:
            logger.warning('--------Error, no energy reducing step found!--------')
            logger.warning('(initial step %f)', step0)
            return (0.0, LS_NO_STEP)
        else:
            logger.info('%d Initial Contraction Steps Taken', it)
    else:
        # expand stepMax until we get an increase in energy
        it = 1
        # force first step
        enAtMid = enAtMax + 1.0
        while \
                enAtMax <= enAtMid and \
                (maxStep == 0 or stepMax < maxStep) and \
                it < MAX_IT:
            stepMid = stepMax
            enAtMid = enAtMax
            stepMax *= PHI
            enAtMax = getEn(sDir, stepMax)
            # print '-- new step max --', stepMax
            it += 1
        if it >= MAX_IT or (maxStep != 0 and stepMax >= maxStep):
            logger.warning('Warning, no energy increasing step found!')
            return (maxStep, LS_MAX_STEP)
        else:
            logger.info('%d Initial Expansion Steps Taken', it)

    bracketDist = getDist(sDir, stepMax - stepMin)
    logger.info('Initial bracket dist = ' + str(bracketDist))

    it = 1
    while bracketDist > eps:
        # TEST
        if enAtMid > enAtMin:
            logger.error('Error, invalid configuration!')
        if enAtMid > enAtMax:
            logger.error('Error, invalid configuration!')
        # END TEST
        if stepMax - stepMid > stepMid - stepMin:
            stepNxt = stepMid + RPHI * (stepMax - stepMid)
        else:
            stepNxt = stepMid - RPHI * (stepMid - stepMin)

        enAtNxt = getEn(sDir, stepNxt)
        if enAtNxt == enAtMid:
            break

        if enAtNxt < enAtMid:
            if stepMax - stepMid > stepMid - stepMin:
                stepMin = stepMid
                enAtMin = enAtMid
                stepMid = stepNxt
                enAtMid = enAtNxt
            else:
                stepMax = stepMid
                enAtMax = enAtMid
                stepMid = stepNxt
                enAtMid = enAtNxt
        else:
            if stepMax - stepMid > stepMid - stepMin:
                stepMax = stepNxt
                enAtMax = enAtNxt
            else:
                stepMin = stepNxt
                enAtMin = enAtNxt
        bracketDist = getDist(sDir, stepMax - stepMin)
        it += 1

    logger.info('%d division steps taken', it)

    # take final step
    en = getEn(sDir, stepMid)

    diffEn = en0 - en

    logger.info('initial/final step: %f/%f', step0, stepMid)

    if diffEn >= 0:
        logger.info('initial/final/diff energy: %f/%f/%f', en0, en, diffEn)
    else:
        logger.error('Error, increased energy!!')

    return (stepMid, LS_OKAY)

# End lineSearch


def BackoffStep(sDir, step, getEn, init_en=None,
                backoff=lambda x: x / 2.0,
                max_backoff_steps=None):
    """
    BackoffStep attempts to take a step of size 'step' in direction
    'sDir'.  If increasing energy is detected, the stepsize is reduced
    by the 'backoff' function, by default halving the step size.

    getEn is a function taking the search dir and a step size,and returns
    the evaluation of the objective function.

    init_en is the energy at step=0, ie it is equivalent to
    getEn(sDir,0).  If not specified, this is calculated.
    """

    if init_en is None:
        init_en = getEn(sDir, 0.)

    logger.info('init en: %f', init_en)

    n_backoff_steps = 0
    cur_en = getEn(sDir, step)
    while cur_en > init_en:
        if (step == 0.0 or
            (max_backoff_steps is not None and
             n_backoff_steps >= max_backoff_steps)):
            return 0.0, init_en
        step = backoff(step)
        n_backoff_steps += 1
        cur_en = getEn(sDir, step)
        logger.info('reduced step: %f new en: %f', step, cur_en)

    logger.info('new en: %f', cur_en)

    return step, cur_en

# end BackoffStep

# Just a test, not a general algorithm
if __name__ == '__main__':

    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt

    def f(px, py):
        return (px / 1.0)**2 + (py / 6.0)**2 - 1.0

    def df(px, py):
        return np.array([2.0 * px, py / 3.0])

    delta = 0.025
    x = np.arange(-6.0, 6.0, delta)
    y = np.arange(-6.0, 6.0, delta)
    X, Y = np.meshgrid(x, y)
    Z = f(X, Y)

    plt.figure('objective')
    CS = plt.contour(X, Y, Z, levels=np.linspace(-1.0, 0.0, 20))
    plt.clabel(CS, inline=1, fontsize=10)
    plt.title('Optimization strategy comparison')
    plt.draw()
    plt.show()

    p = np.array([0.1, 5.0])
    phist = [p.copy()]
    step = 0.2
    niters = 100
    for gdit in range(niters):
        g = -1.0 * df(p[0], p[1])
        p = p + step * g
        phist.append(p.copy())
    phist = zip(*phist)

    plt.figure('objective')
    plt.hold(True)
    gdline, = plt.plot(phist[0], phist[1], 'r-o')
    plt.hold(False)
    plt.draw()
    plt.show()

    p = np.array([0.1, 5.0])
    pNext = p.copy()

    def getEn(sDir, step):
        pNext[:] = p + step * sDir
        return f(pNext[0], pNext[1])

    def getDist(sDir, step):
        return np.sqrt(np.sum((step * sDir)**2))

    phist = [p.copy()]
    step0 = 0.1

    flag = LS_OKAY
    lsit = 0
    while flag != LS_NO_STEP:
        sDir = -1.0 * df(p[0], p[1])
        step, flag = LineSearch(
            sDir, step0, getEn, getDist, eps=1e-6, maxStep=0)
        step0 = 2 * step
        tmp = p
        p = pNext
        pNext = tmp
        print step, p
        phist.append(p.copy())
        lsit += 1
    phist = zip(*phist)

    plt.figure('objective')
    plt.hold(True)
    lsline, = plt.plot(phist[0], phist[1], 'b-o')
    plt.hold(False)
    plt.draw()
    plt.show()

    global CGReset
    CGReset = True

    def ConjugateGradientStep(sDir, g, gPrev, step0, getEn, getDist,
                              eps=1e-6, maxStep=0.0):
        global CGReset

        # compute:
        # dotA = dot(g^n,g^n),
        # dotB = dot(g^n,g^{n-1}), and
        # dotC = dot(g^{n-1},g^{n-1})

        # compute cgBeta
        if CGReset:
            print 'CG Resetting'
            cgBeta = 0.0
            CGReset = False
        else:
            dotA = np.sum(g * g)
            dotB = np.sum(g * gPrev)
            dotC = np.sum(gPrev * gPrev)
            if True:
                # Fletcher-Reeves:
                cgBeta = dotA / dotC
            else:
                # Polak-Ribiere:
                cgBeta = (dotA - dotB) / dotC
                cgBeta = max((0, cgBeta))
            print 'cgBeta: ', cgBeta

        # update search direction
        sDir[:] = g + cgBeta * sDir

        # save current gradient as previous
        gPrev[:] = g

        # do line search update
        step, flag = \
            LineSearch(sDir, step0, getEn, getDist, eps=eps, maxStep=maxStep)

        if flag == LS_MAX_STEP:
            print '----MAX STEP----'
            CGReset = True

        return step, flag

    p = np.array([0.1, 5.0])
    pNext = p.copy()
    g = p.copy()
    gPrev = p.copy()
    sDir = p.copy()

    phist = [p.copy()]
    step0 = 0.1

    flag = LS_OKAY
    cgit = 0
    while flag != LS_NO_STEP:
        if cgit % 100 == 0:
            CGReset = True
        g = -1.0 * df(p[0], p[1])
        step, flag = \
            ConjugateGradientStep(sDir, g, gPrev, step0,
                                  getEn, getDist, eps=1e-6, maxStep=0)
        step0 = 2 * step
        tmp = p
        p = pNext
        pNext = tmp
        print step, p
        phist.append(p.copy())
        cgit += 1
    phist = zip(*phist)

    plt.figure('objective')
    plt.hold(True)
    ncgline, = plt.plot(phist[0], phist[1], 'g-o')
    plt.hold(False)
    plt.draw()
    plt.show()

    plt.figure('objective')
    plt.legend([gdline, lsline, ncgline], ['GD', 'LS', 'NCG'])
    plt.draw()
    plt.show()

    print 'ls/cg iterations %d/%d' % (lsit, cgit)
