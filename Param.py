"""
Param file system using YAML or XML with validation
run unit tests: python -m unittest -v Param

"""

import yaml
import xml.etree.ElementTree as ET
import os.path
import collections
import copy
import unittest
import re
# from PyCASamExtras
import Enum
import Debug
try:
    from treedict import TreeDict
except ImportError:
    TreeDict = None
    print 'warning, TreeDict functionality disabled in Param'

ParamImportance = Enum.Enum('REQUIRED','COMMON','RARE','DEBUG')

# default may be needed separate from None value
class DefaultType:
    pass

DEFAULT = DefaultType()

class ParamConstructError(Exception):
    """Exception type for parameter construction"""
    def __init__(self, value=None):
        super(ParamConstructError, self).__init__(self, value)
        self.value = value

    def __str__(self):
        return repr(self.value)

class ParamParseError(Exception):
    """Exception type for parameter parsing"""
    def __init__(self, value=None):
        super(ParamParseError, self).__init__(self, value)
        self.value = value

    def __str__(self):
        return repr(self.value)

class NamedList(list):
    """
    Used to create list of TreeDict objects for ListParam's toTreeDict
    method, or for initializing a ListParam in a TreeDict hierarchy
    """
    def __init__(self, name=None, val=None, td=None, nEntries=None):
        """
        Name specifies the name of this ListParam parameter.
        """
        if val is not None:
            super(NamedList, self).__init__(val)
            
        self.name = name

        if td is None and val is not None:
            td = val[0]

        self.td = td
        if type(self.td) is TreeDict or \
           type(self.td) is NamedList:
            self.td = self.td.copy(deep=True)
        
        if nEntries is not None:
            for _ in range(nEntries):
                self.init_back()

    def init_back(self, n=1):
        """
        Initialize a new TreeDict element at end of list
        """
        for _ in range(n):
            if type(self.td) is TreeDict or \
               type(self.td) is NamedList:
                self.append(self.td.copy(deep=True))
            else:
                self.append(TreeDict(self.td))

    def copy(self, deep=False):
        nl = NamedList(name=self.name, td=self.td)
        for el in self:
            nl.append(el.copy(deep=True))
        return nl
            

class Param(object):
    """
    Simple parameter class

    name can either be a string (naming the parameter) or a list of
    strings representing acceptable names.  The first entry will be
    the default name.

    Should give a default value which is used for the skeleton file and is the
    default if not required and not provided by the user

    The rare and debug flags just control whether or not this option is shown
    in sample output
    """

    def __init__(self, 
                 name, 
                 value=None, 
                 importance=ParamImportance.COMMON, 
                 comment=None):
        self.setName(name)
            
        self.value = value
        self.importance = importance
        self.comment = comment

    def required(self):
        """Returns True if parameter is required to be set by user"""
        return self.importance == ParamImportance.REQUIRED

    def description(self, multiline=True):
        """Generate string description of param from comment"""
        s = ''
        if self.required():
            s += 'REQUIRED '
        if self.comment is not None:
            s += self.comment
        return s

    def set(self, val):
        self.value = val

    def setName(self, name):
        if type(name) is str:
            self.pnames = [name]
        elif isinstance(name, collections.Iterable):
            self.pnames = []
            for n in name:
                self.addName(n)
        else:
            raise Exception('Error, name entry not a string or list of '
                            'strings: %r'%name)

    def defaultName(self):
        return self.pnames[0]
        
    def addName(self, name, default=False):
        """
        Add name to list of names this parameter will match.  If
        default=True, this name will be the default name if the
        parameter is printed.
        """
        if type(name) != str:
            raise Exception('Error, name entry not a string: %r'%name)
        if default:
            self.pnames.insert(0,name)
        else:
            self.pnames.append(name)
        
    def matchesName(self, name):
        """Returns True if `name` is a name for this param"""

        # DEFAULT matches anything
        if name is DEFAULT:
            return True

        if type(name) is not str:
            raise Exception('Error, name not a string: %r'%name)

        return name in self.pnames
            
    def matchesAnyName(self, nameList):
        """Returns matching name from nameList if found, otherwise None"""

        for n in nameList:
            if self.matchesName(n):
                return n
        return None
            
    def matchesParamName(self, paramOb):
        """Returns matching name from paramOb if found, otherwise None"""

        return self.matchesAnyName(paramOb.pnames)
            
    def removeTopLevel(self, ob):
        """When parsing the top level object from a param file, an
        extra step is neccesary"""
        if type(ob) is dict:
            # At the top level there should only be one key corresponding
            # to the param we're parsing
            assert len(ob) == 1
            name,subdict = ob.iteritems().next()
            assert self.matchesName(name)
            subob = (name, subdict)
        elif type(ob) is ET.ElementTree:
            # get the top-level (root) element from the tree
            el = ob.getroot()
            assert self.matchesName(el.tag)
            subob = el
        else:
            raise ParamParseError('Unexpected type for top-level object: %s' %\
                                      type(ob).__name__)
        return subob

    def parseXMLString(self, xmlString):
        """parse parameters from xml string"""
        paramOb = ET.fromstring(xmlString)
        return self.configuredCopy(paramOb)

    def parseYAMLString(self, yamlString):
        """parse parameters from yaml string"""
        paramOb = yaml.load(yamlString)
        paramOb = self.removeTopLevel(paramOb)
        return self.configuredCopy(paramOb)

    def parseParamFile(self, paramFile):
        """Load a yaml or xml file and insert default values.
        Determine filetype from extension"""
        with open(paramFile, 'r') as f:
            if paramFile.endswith('.xml'):
                paramOb = ET.parse(f)
            else:
                paramOb = yaml.load(f, Loader=IncludeLoader)

        paramOb = self.removeTopLevel(paramOb)

        return self.configuredCopy(paramOb)

    def configuredCopy(self, ob=DEFAULT):
        """Create a copy of the object with values parsed from ob"""

        newParam = copy.deepcopy(self)
        if ob is not DEFAULT and \
               (type(ob) is not ET.Element and
                type(ob) is not TreeDict and
                type(ob) is not ParamList):
            if \
                    len(ob) != 2 or \
                    type(ob) is not tuple or \
                    not self.matchesName(ob[0]):
                # create (name, dict) tuple so content dict can be passed
                # directly to configuredCopy
                ob = (self.defaultName(), ob)

        newParam.parse(ob)
            
        return newParam

    def parse(self, ob):
        """Given the object for this parameter, validate and parse it
        and return parsed param object"""

        if type(ob) is ET.Element:
            self.doValidateXML(ob)
            self.doParseXML(ob)
        elif type(ob) is TreeDict \
             or type(ob) is NamedList:
            self.doValidateTD(ob)
            self.doParseTD(ob)
        else:
            self.doValidateDict(ob)
            self.doParseDict(ob)
                                    
    def doValidateDict(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        if ob is not DEFAULT:

            # ob should be a name/value pair
            if len(ob) != 2:
                raise ParamParseError('dict ob not name/value pair: %r'%ob)

            name = ob[0]
            val = ob[1]
            
            if not self.matchesName(name):
                raise ParamParseError('name does not match: %r and %r'%
                                      (name, self.defaultName()))
        elif self.required():

            raise ParamParseError('Required param %s not defined'
                                  %self.defaultName())

    def doParseDict(self, ob):
        """Parse object
        
        override in subclasses"""

        if ob is not DEFAULT:
            val = ob[1]
            self.value = copy.deepcopy(val)

    def doValidateTD(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        if ob is DEFAULT:
            if self.required():
                raise ParamParseError('Required param %s not defined'
                                      %self.defaultName())
        else:
            # Make sure the ob is a TreeDict or NamedList
            if not isinstance(ob, TreeDict) and \
                   not isinstance(ob, NamedList):
                raise ParamParseError('ParentParam expects TreeDict or NamedList object, '
                                      'got %s'%val.__type__)
        
            if type(ob) is TreeDict:
                name = ob.branchName()
                if len(name) == 0:
                    name = ob.treeName()
            elif type(ob) is NamedList:
                name = ob.name
            else:
                raise ParamParseError('unrecognized type for doValidateTD: %r'%ob)
            if not self.matchesName(name):
                raise ParamParseError('name does not match: %r and %r'%
                                      (name, self.defaultName()))

    def doParseTD(self, ob):
        # values should be parsed as dicts, even in TreeParam
        # hierarchies.
        raise ParamParseError('Error, doParseTD should never be called in Param')

    def getXMLLeafValue(self, el):
        """Return the 'val' attribute or element text"""
        if len(el):
            raise ParamParseError('element %s should be leaf, '
                                  'but has children' % el.tag)

        if len(el.attrib):
            # parse value out of 'val' attribute
            valstr = el.attrib.get('val')
            if valstr is None or len(el.attrib) > 1:
                raise ParamParseError('XML element %s should only have `val` '
                                      'attribute'%el.tag)
        else:
            # parse value out of element text
            valstr = el.text
            if valstr is None:
                raise ParamParseError('leaf XML element %s has neither val '
                                      'attribute nor text' % el.tag)

        try:
            val = eval(valstr)
        except NameError, SyntaxError:
            print('Warning, unable to eval "%s" from xml element %s -- '
                  'assuming this a string that should have been quoted'\
                      %(valstr, el.tag))
            val = valstr

        return val

    def doValidateXML(self, el):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""
        
        if el is DEFAULT:
            if self.required():
                raise ParamParseError('Required param %s not defined' % \
                                          self.defaultName())
        else:
            # Make sure the ob is an Element
            if not isinstance(el, ET.Element):
                raise ParamParseError('Param expects Element object, '
                                      'got %s'%ob.__type__)
        
    def doParseXML(self, el):
        """Parse XML ElementTree element
        
        override in subclasses"""

        if el is not DEFAULT:
            self.value = self.getXMLLeafValue(el)

    def indent(self, level=0):
        return '    '*level;

    def yamlComment(self, comment, level=0):
        return \
            ''.join([self.indent(level)+'# '+line+'\n'
                     for line in comment.strip().split('\n')])

    def xmlComment(self, comment, level=0):
        comment = comment.strip()
        outstr = self.indent(level) + '<!-- '
        if comment.find('\n') < 0:
            outstr += comment + ' -->\n'
        else:
            outstr += '\n'
            outstr += ''.join([self.indent(level+1)+line+'\n'
                               for line in comment.split('\n')])
            outstr += self.indent(level) + '-->\n'

        return outstr

    def toYAML(self, multiline=True, level=0):
        """Output as YAML text"""
        s = ''
        desc = self.description(multiline=multiline)
        if multiline:
            if len(desc):
                s += self.yamlComment(comment=desc, level=level)
            # the way to write None is a missing key in a dict, so
            # comment out the key
            s += self.indent(level)
            if self.value == None:
                s += '# '
            s += '%s: %s'%(self.defaultName(),str(self.value))
        else:
            # the way to write None is a missing key in a dict, so
            # comment out the key
            if self.value == None:
                s += '# '
            s += '%s: %s'%(self.defaultName(),str(self.value))
            if len(desc):
                s += '  # ' + desc
        return s

    def toXML(self, level=0):
        """Output as XML text"""
        s = ''
        desc = self.description()
        if len(desc):
            s += self.xmlComment(comment=desc, level=level)
        s += self.indent(level) + '<%s val="%r" />\n'%(self.defaultName(),
                                                       self.value)
        return s

    def toTreeDict(self, parent=None, deepcopy=False):
        val = self.value
        if deepcopy:
            val = copy.deepcopy(val)
        if parent is not None:
            for name in self.pnames:
                parent.__setattr__(name, val)
        return val

    def __str__(self):
        return self.toYAML(multiline=False)

    # this is how you get the value of the param
    def __call__(self):
        return self.value

    # we'll need to be able to make copies of Param objects
    def __copy__(self):
        new_p = Param(name=self.pnames, 
                      value=self.value, 
                      importance=self.importance, 
                      comment=self.comment)
        return new_p

    def __deepcopy__(self, memo):
        new_p = Param(name=self.pnames, 
                      value=copy.deepcopy(self.value, memo), 
                      importance=self.importance, 
                      comment=self.comment)
        return new_p

# end class Param

# convenience functions for list of params
class ParamList(object):

    def __init__(self, 
                 entries=[]):

        if not isinstance(entries, collections.Iterable):
            raise ParamConstructError('ParamList recieved non-iterable '
                                      '`entries` param')
        
        # create `entries` dict mapping name to child param
        self.entries = []
        for entry in entries:
            self.addEntry(entry)

    def addEntry(self, entry, name=None):
        """
        add parameter 'entry'.  If optional 'name' is specified,
        rename the entry to this name / these names
        """

        # add a copy, not the original
        entry = copy.deepcopy(entry)
        # rename if requested
        if name is not None:
            entry.setName(name)
        
        if not isinstance(entry, Param):
            raise ParamConstructError('ParamList recieved non-Param '
                                      'entry')
        if self.contains(entry.defaultName()):
            raise ParamConstructError('Entry %s multiply-defined' % \
                                (entry.defaultName()))

        self.entries.append(entry)
        

    def contains(self, name):
        return self.get(name) is not None

    def get(self, name):
        
        for entry in self.entries:
            if entry.matchesName(name):
                return entry
        return None

    def getAny(self, nameList):
        
        for entry in self.entries:
            name = entry.matchesAnyName(nameList)
            if name is not None:
                return name, entry
        return None, None

    # implementation of container functionality, just pass through to
    # 'self.entries', which is a list
    def __len__(self):
        return self.entries.__len__()

    def __getitem__(self, key):
        if type(key) is int:
            return self.entries.__getitem__(key)
        elif type(key) is str:
            return self.get(key)
        else:
            raise KeyError('%r'%key)

    def __iter__(self):
        return self.entries.__iter__()


class ParentParam(Param):
    """
    Parameter class that parses a dictionary into a list of child params
    """

    def __init__(self, 
                 name, 
                 children=[], 
                 importance=ParamImportance.COMMON, 
                 comment=None):

        super(ParentParam, self).__init__(name=name, 
                                          value=None, 
                                          importance=importance,
                                          comment=comment)

        self.children = ParamList(children)

        for k in self.__dict__.keys():
            if self.children.contains(k):
                raise ParamConstructError('Child param ' + k + 
                                          ' exists as attribute.' +
                                          ' Error in param spec')

    def addChild(self, child, name=None):
        """
        Add a child to this parent.  If optional 'name' is specified,
        rename the entry to this name / these names
        """
        
        self.children.addEntry(child, name=name)

    def doValidateDict(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(ParentParam, self).doValidateDict(ob)

        if ob is DEFAULT:
            return

        name = ob[0]
        val = ob[1]
        
        # Make sure the ob is a dict
        if not isinstance(val, dict):
            raise ParamParseError('ParentParam expects dict object, '
                                  'got %s'%val.__type__)
        
        # Make sure there aren't any children in ob that aren't in the
        # param
        for k in val.keys():
            if not self.children.contains(k):
                raise ParamParseError('Got key not defined in param: %s'%k)

    def doParseDict(self, ob):
        """Parse child params recursively, expects a dict"""

        # just use default values
        if ob is DEFAULT:
            val = {}
        else:
            val = ob[1]

        for child in self.children:
            k = child.matchesAnyName(val.keys())
            if k is not None:
                child.parse((k, val[k]))
            else:
                # parse as default
                child.parse(DEFAULT)

    def doValidateTD(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(ParentParam, self).doValidateTD(ob)
        
        if ob is DEFAULT:
            return
        
        # Make sure the ob is a TreeDict
        if not isinstance(ob, TreeDict):
            raise ParamParseError('ParentParam expects TreeDict object, '
                                  'got %s'%val.__type__)
            
        # Make sure there aren't any children in ob that aren't in the
        # param
        for k in ob.keys(recursive=False, branch_mode='all'):
            if not self.children.contains(k):
                raise ParamParseError('Got key not defined in param: %s'%k)
                
    def doParseTD(self, ob):
        """Parse child params recursively, expects a TreeDict"""

        # just use default values
        if ob is DEFAULT:
            ob = TreeDict(self.defaultName())

        for child in self.children:
            branchNames = ob.keys(recursive=False, branch_mode='only')
            valNames = ob.keys(recursive=False, branch_mode='none')
            k = child.matchesAnyName(branchNames)
            # parse if branch
            if k is not None:
                child.parse(ob[k])
                continue
            # parse if val
            k = child.matchesAnyName(valNames)
            if k is not None:
                v = ob[k]
                if type(v) is NamedList or type(v) is TreeDict:
                    child.parse(v)
                else:
                    child.parse((k, v))
                continue
            # parse as default
            child.parse(DEFAULT)

    def doValidateXML(self, el):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(ParentParam, self).doValidateXML(el)

        # just use default values
        if el is DEFAULT:
            return

        # Make sure there aren't any children in ob that aren't in the
        # param
        for child in el:
            if not self.children.contains(child.tag):
                raise ParamParseError('Got key not defined in param: %s'%\
                                          child.tag)
        

    def doParseXML(self, el):
        """Parse child params recursively, expects a dict"""

        # just use default values
        if el is DEFAULT:
            # element with no children
            el = ET.Element(self.defaultName())

        for child in self.children:
            for child_el in el:
                if child.matchesName(child_el.tag):
                    child.parse(child_el)
                    break
            else:
                child.parse(DEFAULT)

    def toYAML(self, multiline=True, level=0):
        """Output as YAML example"""
        s = ''
        desc = self.description(multiline=multiline)
        if multiline:
            if len(desc):
                s += self.yamlComment(comment=desc, level=level)
            s += self.indent(level) + self.defaultName() + ':\n'
        else:
            s += self.indent(level) + self.defaultName() + ':'
            if len(desc):
                ' #' + desc
            s += '\n'

        for child in self.children:
            s += child.toYAML(multiline=multiline, level=level+1) + '\n'
        return s

    def toXML(self, level=0):
        """Output as XML example"""
        desc = self.description()
        s = ''
        if len(desc):
            s += self.xmlComment(comment=desc, level=level)
        s += self.indent(level) + '<' + self.defaultName() + '>\n'
        for child in self.children:
            s += child.toXML(level=level+1)
        s += self.indent(level) + '</' + self.defaultName() + '>\n'
        return s

    def toTreeDict(self, parent=None, deepcopy=False):
        td = TreeDict(self.defaultName())
        for child in self.children:
            child.toTreeDict(parent=td, deepcopy=deepcopy)
        if parent is not None:
            for name in self.pnames:
                parent.__setattr__(name, td)
            # convert tree instances into branches
            parent.attach(recursive=True)
        return td

    # we'll need to be able to make copies of ParentParam objects
    def __copy__(self):
        new_p = ParentParam(name=self.defaultName(), 
                            children=self.children,
                            importance=self.importance, 
                            comment=self.comment)
        return new_p

    def __deepcopy__(self, memo):
        new_children = []
        for child in self.children:
            new_children.append(copy.deepcopy(child))
            
        new_p = ParentParam(name=self.defaultName(), 
                            children=new_children, 
                            importance=self.importance, 
                            comment=self.comment)
        return new_p

    def __getattr__(self, name):
        child = self.children[name]
        if child is not None:
            return child
        else:
            raise AttributeError('%r object has no attribute %r'%(self.__class__.__name__, name))
        

# end class ParentParam

class ListParam(Param):
    """
    Parameter class that parses a list of the same type of child params
    """

    def __init__(self, 
                 name, 
                 value=None, 
                 template=None,
                 importance=ParamImportance.COMMON, 
                 comment=None,
                 updateDefaultOnParse=True):
        super(ListParam, self).__init__(name=name, 
                                        value=None, 
                                        importance=importance,
                                        comment=comment)

        self.updateDefaultOnParse = updateDefaultOnParse

        # make sure template is valid
        if template is not None and \
                not isinstance(template, Param):
            raise ParamConstructError('ListParam recieved non-Param '
                                      'template')
        self.template = template

        if isinstance(value, Param):
            # got a single instance param for this list
            self.value = [value]
            if self.template is None:
                self.template = value
        elif isinstance(value, collections.Iterable):
            # make sure we got a non-empty list with entries that are
            # all of the same type, and this type is a subclass of
            # Param

            if not len(value):
                raise ParamConstructError('ListParam got empty list as value')

            # if not specified, use last item as template (or should this
            # be first?)
            if self.template is None:
                self.template = value[-1]

            # do some checking on the values, enter into a list
            self.value = []
            for ob in value:
                if not isinstance(ob, Param):
                    raise ParamConstructError('ListParam recieved non-Param '
                                              'value')
                if not self.template.matchesParamName(ob):
                    raise ParamConstructError('ListParam recieved parameters '
                                              'with different names: '
                                              '%s and %s' % \
                                                  (self.template.defaultName(), ob.defaultName()))
                # everything checks out, append
                self.value.append(ob)
        else:
            raise ParamConstructError('ListParam recieved non-Param, '
                                      'non-iterable value value')
            

    def doValidateDict(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(ListParam, self).doValidateDict(ob)

        if ob is DEFAULT:
            return

        val = ob[1]

        # Make sure the val is a list
        if not isinstance(val, list):
            raise ParamParseError('ListParam expects list object, '
                                  'got %s'%val.__type__)
        # these should all be 1-entry dicts (right??) 
        for item in val:
            if not isinstance(item, dict):
                raise ParamParseError('ListParam entries should be dicts,'
                                      'got %s'%item.__type__)
            
            if len(item) != 1:
                raise ParamParseError('ListParam entry should be 1-entry '
                                      'dicts, got %d-entry dict'%len(item))
            
            name,subdict = item.iteritems().next()
            if not self.template.matchesName(name):
                raise ParamParseError('Got wrong name for entry, doesnt '
                                      'match template (%s != %s)'%\
                                          (name, self.template.defaultName()))

    def doParseDict(self, ob):
        """Parse child params recursively, expects a list"""
        
        # just use default values
        if ob is DEFAULT:
            return

        val = ob[1]

        # if any items are parsed, don't use the default values
        self.value = []
        # note, if updateDefaultOnParse is True, template is updated
        # to parsed values each time
        for item in val:
            # copy template and parse values
            entry = copy.deepcopy(self.template)
            entry.parse(item.items()[0])
            self.value.append(entry)
            if self.updateDefaultOnParse:
                self.template = entry

    def doValidateTD(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(ListParam, self).doValidateTD(ob)
        
        if ob is DEFAULT:
            return

        # Make sure the ob is a NamedList
        if not isinstance(ob, NamedList):
            raise ParamParseError('ListParam expects NamedList object, '
                              'got %s'%ob.__type__)
            
        # these should all be TreeDicts or NamedLists
        for item in ob:
            if not isinstance(item, TreeDict) and \
                not isinstance(item, NamedList):
                raise ParamParseError('ListParam entries should be TreeDicts or NamedLists,'
                                      'got %s'%item.__type__)
            
            if type(item) is TreeDict:
                name = item.branchName()
                if len(name) == 0:
                    name = item.treeName()
            elif type(item) is NamedList:
                name = item.name
            else:
                raise ParamParseError('unrecognized type for doValidateTD: %r'%ob)
            if not self.template.matchesName(name):
                raise ParamParseError('Got wrong name for entry, doesnt '
                                      'match template (%s != %s)'%\
                                          (name, self.template.defaultName()))

    def doParseTD(self, ob):
        """Parse child params recursively, expects a list"""
        
        # just use default values
        if ob is DEFAULT:
            return

        # if any items are parsed, don't use the default values
        self.value = []
        # note, if updateDefaultOnParse is True, template is updated
        # to parsed values each time
        for item in ob:
            # copy template and parse values
            entry = copy.deepcopy(self.template)
            entry.parse(item)
            self.value.append(entry)
            if self.updateDefaultOnParse:
                self.template = entry

    def doValidateXML(self, el):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(ListParam, self).doValidateXML(el)

        if el is DEFAULT:
            return

        for child in el:
            if not self.template.matchesName(child.tag):
                raise ParamParseError('Got wrong name for entry, doesnt '
                                      'match template (%s != %s)'% \
                                          (child.tag, self.template.defaultName()))
        
    def doParseXML(self, el):
        """Parse child params recursively, expects a list"""
        
        # just use default values
        if el is DEFAULT:
            return

        # if any items are parsed, don't use the default values
        self.value = []
        # note, template is updated to parsed values each time
        for child in el:
            # copy template and parse values
            entry = copy.deepcopy(self.template)
            entry.parse(child)
            self.value.append(entry)
            if self.updateDefaultOnParse:
                self.template = entry

    def toYAML(self, multiline=True, level=0):
        """Output as YAML example"""
        desc = self.description(multiline=multiline)
        s = ''
        if multiline:
            if len(desc):
                s += self.yamlComment(comment=desc, level=level)
            s += self.indent(level) + self.defaultName() + ':\n'
        else:
            s += self.indent(level) + self.defaultName() + ':'
            if len(desc):
                ' #' + desc
            s += '\n'
        for p in self.value:
            s += self.indent(level) + '-\n'
            s += p.toYAML(multiline=multiline, level=level+1) + '\n'
        return s

    def toXML(self, level=0):
        """Output as XML example"""
        desc = self.description()
        s = ''
        if len(desc):
            s += self.xmlComment(comment=desc, level=level)
        s += self.indent(level) + '<' + self.defaultName() + '>\n'
        for p in self.value:
            s += p.toXML(level=level+1) + '\n'
        s += self.indent(level) + '</' + self.defaultName() + '>\n'
        return s

    def toTreeDict(self, parent=None, deepcopy=False):
        tdlist = NamedList(self.defaultName())
        for p in self.value:
            val = p.toTreeDict(None, deepcopy=deepcopy)
            tdlist.append(val)
        if parent is not None:
            for name in self.pnames:
                parent.__setattr__(name, tdlist)
        return tdlist
    
    # implementation of container functionality, just pass through to
    # 'self.value', which is a list
    def __len__(self):
        return self.value.__len__()

    def __getitem__(self, key):
        return self.value.__getitem__(key)

    def __iter__(self):
        return self.value.__iter__()

    # note, mutable container functions not implemented (__setitem__,
    # __delitem__)

    # we'll need to be able to make copies of ListParam objects
    def __copy__(self):
        new_p = ListParam(name=self.pnames, 
                          value=self.value,
                          template=self.template,
                          importance=self.importance, 
                          comment=self.comment,
                          updateDefaultOnParse=self.updateDefaultOnParse)
        return new_p

    def __deepcopy__(self, memo):
        new_value = []
        for v in self.value:
            new_value.append(copy.deepcopy(v))

        new_template = copy.deepcopy(self.template)
            
        new_p = ListParam(name=self.pnames, 
                          value=new_value, 
                          template=new_template, 
                          importance=self.importance, 
                          comment=self.comment,
                          updateDefaultOnParse=self.updateDefaultOnParse)
        return new_p

# end class ListParam

class EnumParam(Param):
    """
    Parameter class that parses one of a list of possible values
    """

    def __init__(self, 
                 name, 
                 enumType,
                 value, 
                 importance=ParamImportance.COMMON, 
                 comment=None):
        super(EnumParam, self).__init__(name=name, 
                                        value=value, 
                                        importance=importance,
                                        comment=comment)
        self.enumType = enumType
            
        
    def description(self, multiline=True):
        """Generate string description of param from comment"""
        desc = super(EnumParam, self).description(multiline=multiline)
        if multiline:
            desc += '\nEnum Values:'
            for k in self.enumType.enum_mapping:
                desc += '\n    '+k
        return desc

    def doValidateDict(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(EnumParam, self).doValidateDict(ob)

        if ob is DEFAULT:
            return

        val = ob[1]

        # make sure the value is one of the enum values
        if val not in self.enumType.enum_mapping:
            if val not in self.enumType.reverse_mapping:
                raise ParamParseError('%r does not exist in enum for '
                                      'EnumParam %s'%(ob, self.defaultName()))
            
    def doParseDict(self, ob):
        """Parse value ob"""
        
        # just use default values
        if ob is DEFAULT:
            return

        val = ob[1]

        if val in self.enumType.enum_mapping:
            self.value = copy.deepcopy(
                self.enumType.enum_mapping[val])
        else:
            print('Warning, setting enum param value directly '
                  '(value=%r) rather than from name'%val)
            self.value = copy.deepcopy(val)

    def doValidateTD(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # EnumParam should never get a TreeDict object, so just raise
        # exception
        raise ParamParseError('EnumParam doValidateTD called, this should not occur (ob is %r)'%ob)
            
    def doParseTD(self, ob):
        """Parse value ob"""
        
        # EnumParam should never get a TreeDict object, so just raise
        # exception
        raise ParamParseError('EnumParam doParseTD called, this should not occur (ob is %r)'%ob)

    def doValidateXML(self, el):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(EnumParam, self).doValidateXML(el)

        if el is DEFAULT:
            return

        val = self.getXMLLeafValue(el)

        # make sure the value is one of the enum values
        if val not in self.enumType.enum_mapping:
            if val not in self.enumType.reverse_mapping:
                raise ParamParseError('%r does not exist in enum for '
                                      'EnumParam %s'%(val, self.defaultName()))
        
    def doParseXML(self, el):
        """Parse child params recursively, expects a list"""
        
        # don't do anything if el is DEFAULT
        if el is not DEFAULT:
            val = self.getXMLLeafValue(el)
            if val in self.enumType.enum_mapping:
                self.value = copy.deepcopy(
                    self.enumType.enum_mapping[val])
            else:
                print('Warning, setting enum param value directly '
                      '(value=%r) rather than from name'%val)
                self.value = copy.deepcopy(val)


    def toYAML(self, multiline=True, level=0):
        """Output as YAML text"""
        # save original value
        saveval = self.value
        # replace value with enum name
        self.value = self.enumType.reverse_mapping[self.value]
        # use superclass function to create output
        outstr = super(EnumParam, self).toYAML(multiline=multiline, level=level)
        # return original value
        self.value = saveval

        return outstr
        
    def toXML(self, level=0):
        """Output as XML text"""
        # save original value
        saveval = self.value
        # replace value with enum name
        self.value = self.enumType.reverse_mapping[self.value]
        # use superclass function to create output
        outstr = super(EnumParam, self).toXML(level=level)
        # return original value
        self.value = saveval

        return outstr
        
    def toTreeDict(self, parent=None, deepcopy=False):
        super(EnumParam, self).toTreeDict(parent, deepcopy=deepcopy)

    # we'll need to be able to make copies of Param objects
    def __copy__(self):
        new_p = EnumParam(name=self.defaultName(), 
                          enumType=self.enumType,
                          value=self.value, 
                          importance=self.importance, 
                          comment=self.comment)
        return new_p

    def __deepcopy__(self, memo):
        new_p = EnumParam(name=self.defaultName(), 
                          enumType=self.enumType,
                          value=copy.deepcopy(self.value, memo), 
                          importance=self.importance, 
                          comment=self.comment)
        return new_p

class XORParam(Param):
    """
    Parameter class that can contain one of multiple types of Param
    """

    def __init__(self, 
                 name, 
                 subparams, 
                 value=None,
                 importance=ParamImportance.COMMON,  
                comment=None):

        super(XORParam, self).__init__(name=name, 
                                       value=value, 
                                       importance=importance,
                                       comment=comment)

        self.subparams = ParamList(subparams)

        # make sure no names conflict with default attributes
        name, param = self.subparams.getAny(self.__dict__.keys())
        if name is not None:
            raise ParamConstructError('subparamparam ' + name + 
                                      ' exists as attribute.' +
                                      ' Error in param spec.')

        if self.value is None:
            self.value = self.subparams[0]
        else:
            self.value = self.subparams.get(self.value.defaultName())

        # add names to which this param responds -- ie all subparam names
        for subp in self.subparams:
            self.pnames.extend(subp.pnames)

    def parsedParam(self):
        return self.value

    def parsedName(self):
        return self.value.defaultName()

    def parsedNameMatches(self, name):
        return self.value.matchesName(name)

    def doValidateDict(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(XORParam, self).doValidateDict(ob)

        if ob is DEFAULT:
            return

        name = ob[0]
        val = ob[1]

        if not self.subparams.contains(name):
            raise ParamParseError('Got name for entry not in subparam '
                                  'list (%s)'%name)
        
    def doParseDict(self, ob):
        """Parse subparam"""
        
        # just use default values
        if ob is DEFAULT:
            return

        # get name and value
        name = ob[0]
        val = ob[1]

        self.value = self.subparams[name]
        self.value.parse((name, val))

    def doValidateTD(self, ob):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(XORParam, self).doValidateTD(ob)

        if ob is DEFAULT:
            return

        if type(ob) is TreeDict:
            name = ob.branchName()
            if len(name) == 0:
                name = ob.treeName()
        elif type(ob) is NamedList:
            name = ob.name
        else:
            raise ParamParseError('unrecognized type for doValidateTD: %r'%ob)

        if not self.subparams.contains(name):
            raise ParamParseError('Got name for entry not in subparam '
                                  'list (%s)'%name)
        
    def doParseTD(self, ob):
        """Parse subparam"""
        
        # just use default values
        if ob is DEFAULT:
            return

        if type(ob) is TreeDict:
            name = ob.branchName()
            if len(name) == 0:
                name = ob.treeName()
        elif type(ob) is NamedList:
            name = ob.name
        else:
            raise ParamParseError('unrecognized type for doValidateTD: %r'%ob)
        
        self.value = self.subparams[name]
        self.value.parse(ob)

    def doValidateXML(self, el):
        """Validate the object to be parsed
        
        raise exception if there
        is a problem, override in subclasses"""

        # superclass validation
        super(XORParam, self).doValidateXML(el)

        if el is DEFAULT:
            return

        if not self.subparams.contains(el.tag):
            raise ParamParseError('Got name for entry not in subparam '
                                  'list (%s)'%el.tag)
            
    def doParseXML(self, el):
        """Parse subparam"""
        
        # just use default values
        if el is DEFAULT:
            return

        self.value = self.subparams[el.tag]
        self.value.parse(el)

    def toYAML(self, multiline=True, level=0):
        """Output as YAML example"""
        if multiline:
            s = self.indent(level) + \
                '# XORParam: Please choose one of the following params\n'

            desc = self.description(multiline=multiline)
            if len(desc):
                s += self.yamlComment(comment=desc, level=level)

            for p in self.subparams:
                subparamYAML = p.toYAML(multiline, level)
                if not p.matchesName(self.value.defaultName()):
                    subparamYAML = self.yamlComment(subparamYAML, level=level)
                s += subparamYAML
            s += self.indent(level) + '# END XORParam'
        else:
            s = self.value.toYAML(multiline, level)

        return s

    def toXML(self, level=0):
        """Output as XML example"""
        return self.value.toXML(level)

    def toTreeDict(self, parent=None, deepcopy=False):
        val = self.value.toTreeDict(parent, deepcopy=deepcopy)
        # for name in self.pnames:
        #     parent.__setattr__(name, val)
        if parent is not None:
            parent.__setattr__(self.defaultName(), val)
        return val
        
    # we'll need to be able to make copies of XORParam objects
    def __copy__(self):
        new_p = XORParam(name=self.pnames, 
                         subparams=self.subparams, 
                         value = self.value,
                         importance=self.importance, 
                         comment=self.comment)
        return new_p

    def __deepcopy__(self, memo):
        new_subparams = []
        for p in self.subparams:
            new_subparams.append(copy.deepcopy(p))

        new_p = XORParam(name=self.pnames, 
                         subparams=new_subparams,
                         value = self.value,
                         importance=self.importance, 
                         comment=self.comment)
        return new_p
    
    # delegate attributes to subparam
    def __getattr__(self, name):
        return getattr(self.value, name)

    # delegate call to subparam
    def __call__(self):
        return self.value()

class IncludeLoader(yaml.Loader):  # pylint: disable-msg=R0901
    """
    Subclassed yaml loader that supports the !include directive
    """
    def __init__(self, stream):
        self._root = os.path.split(stream.name)[0]
        super(IncludeLoader, self).__init__(stream)

    def include(self, node):
        """Include another yaml at this node"""
        filename = os.path.join(self._root, self.construct_scalar(node))
        with open(os.path.expanduser(filename), 'r') as f:
            return yaml.load(f, Loader=IncludeLoader)

IncludeLoader.add_constructor('!include', IncludeLoader.include)

def ParseSimpleParamsFromDict(name, d, comment='', 
                              importance=ParamImportance.COMMON):
    """Parse a ParentParam containing only Param children from dict

    Dict structure has keys containing child param names, and each
    value in the dict is a two-element dict with keys 'value',
    containing the default value, and 'comment', containing the
    comment.  Optionally, a third key 'importance' is also accepted
    containing the importance level (defaults to `common` if left
    unspecified)"""
    
    paramList = []

    if type(d) is not dict:
        raise ParamConstructError('invalid parameter (%s), `d` must be dict'%\
                                      name)
    
    # deep copy, we'll pop elements from sub-dicts of d
    d = copy.deepcopy(d)

    for p_name, p_dict in d.iteritems():

        if type(p_dict) is not dict:
            raise ParamConstructError('parsing %s, invalid value for '
                                      'key %s, must be dict'%(name,p_name))

        BadVal = 'DNE'
        p_value = p_dict.pop('value', BadVal)
        if p_value is BadVal:
            raise ParamConstructError('invalid dict structure (%s), children '
                                      'must have `value` key defined'%p_name)

        p_comment = p_dict.pop('comment', BadVal)
        if p_comment is BadVal:
            raise ParamConstructError('invalid dict structure (%s), children '
                                      'must have `comment` key defined'%p_name)

        p_importance = p_dict.pop('importance', BadVal)
        if p_importance is BadVal:
            p_importance = ParamImportance.COMMON

        if len(p_dict):
            raise ParamConstructError('Error, unknown keys parsing %s:%s'%\
                                          (name,p_name))

        p = Param(name=p_name, value=p_value, 
                  comment=p_comment, importance=p_importance)
        paramList.append(p)

    pp = ParentParam(name=name, children=paramList, 
                     comment=comment, importance=importance)
    
    return pp
        

class MissingConfigError(Exception):
    """Exception type for missing config file"""
    def __init__(self, value=None):
        super(MissingConfigError, self).__init__(self, value)
        self.value = value

    def __str__(self):
        return repr(self.value)

def Load(param, argv):
    """Parse parameters from YAML file
    """
    if len(argv) < 2:
        print('# Usage: ' + argv[0] + ' <config.yaml>')
        print('# Below is a sample config YAML file')
        print(param.toYAML())
        # print('# PyCA Version: %s (%s)' % (common.PyCAVersion(),
        #                                    common.PyCABranch()))
        if '_resource' in param:
            # this won't be printed by default, but let's keep track
            print('_resource: ' + param['_resource'])

        raise MissingConfigError()
    paramFile = argv[1]
    ob = LoadParamFile(paramFile)
    return MkConfig(ob, param)

#
# Test Class
#
class ParamTestCase(unittest.TestCase):

    NumberEnum = Enum.Enum('Zero', 'One', 'Two', 'Three', 'Four')
    
    letterChildren = [
        Param(name='a', value='A', 
              comment='the letter A'),
        Param(name='b', value='B', 
              comment='the letter B'),
        Param(name='c', value='C', 
              comment='the letter C'),
        Param(name='d', value='D', 
              comment='the letter D'),
        Param(name='e', value='E', 
              comment='the letter E'),
        Param(name='f', value='F', 
              comment='the letter F')]

    # empty parent params for use in XORParam
    letterParentParamChildren = [
        ParentParam(name='a', children=[], 
                    comment='the letter A'),
        ParentParam(name='b', children=[], 
                    comment='the letter B'),
        ParentParam(name='c', children=[], 
                    comment='the letter C'),
        ParentParam(name='d', children=[], 
                    comment='the letter D'),
        ParentParam(name='e', children=[], 
                    comment='the letter E'),
        ParentParam(name='f', children=[], 
                    comment='the letter F')]
    

    def __init__(self, methodName='runTest'):
        super(ParamTestCase, self).__init__(methodName)

    def getLetterParentParam(self):
        lc = copy.deepcopy(self.letterChildren)
        LetterParam = \
            ParentParam(name='letter', \
                            children=lc, \
                            comment='Letters of the Alphabet')
        return LetterParam

    def getLetterXORParam(self):
        LetterParam = XORParam(name='LetterXORParam', 
                               subparams=self.letterChildren,
                               comment='choice of letter')
        return LetterParam

    def getLettersParam(self):
        """
        List of XORParams
        """
        LetterOptParam = XORParam(name='LetterOpt', 
                                  subparams=self.letterParentParamChildren,
                                  comment='choice of letter')
        LettersParam = \
            ListParam(name='Letters', value=[LetterOptParam],
                      comment='list of letters')
        return LettersParam
        
    def getNumberEnumParam(self):
        NumberParam = \
            EnumParam(name='NumberEnum', 
                      enumType=self.NumberEnum,
                      value=self.NumberEnum.One,
                      comment='numbers [0...4]')
        return NumberParam

    def getPeopleListParam(self):
        """
        list param
        """
        personDict = { 'name': {'value': 'unknownName',
                                'comment': 'full name'},
                       'age': {'value': 0,
                               'comment': 'age'}}

        personParam = \
            ParseSimpleParamsFromDict('person', \
                                      personDict, \
                                      comment='entry for person', \
                                      importance=ParamImportance.COMMON)

        personList = [ 
            personParam.configuredCopy(
                {'name':'John Doe',
                 'age': 33}),
            personParam.configuredCopy(
                {'name':'Adam Smith',
                 'age': 28})]

        peopleParam = \
            ListParam(name='people', value=personList, \
                          comment='list of people')

        return peopleParam

    def getTeamListParam(self):
        """
        List-of-lists param
        """
        peopleParam = self.getPeopleListParam()
        teamParam = \
            ListParam(name='team', value=[peopleParam],
                      comment='lists of people')
        return teamParam
        
    def test_ParseSimpleParamsFromDict(self):

        letterParamDict = {
            'a': {'value': 'A', 
                  'comment': 'the letter A'},
            'b': {'value': 'B', 
                  'comment': 'the letter B'},
            'c': {'value': 'C', 
                  'comment': 'the letter C'}}

        letterParam = \
            ParseSimpleParamsFromDict('letters', \
                                          letterParamDict, \
                                          comment='some letters', \
                                          importance=ParamImportance.COMMON)

        self.assertEqual(letterParamDict['a']['value'], 
                         letterParam.a())
        self.assertEqual(letterParamDict['a']['comment'], 
                         letterParam.a.comment)
        self.assertEqual(letterParamDict['b']['value'], 
                         letterParam.b())
        self.assertEqual(letterParamDict['b']['comment'], 
                         letterParam.b.comment)
        self.assertEqual(letterParamDict['c']['value'], 
                         letterParam.c())
        self.assertEqual(letterParamDict['c']['comment'], 
                         letterParam.c.comment)
        
    def test_ParentParamDictParse(self):

        letterParam = self.getLetterParentParam()
        
        self.assertEqual(letterParam.a(), 'A')
        self.assertEqual(letterParam.b(), 'B')
        self.assertEqual(letterParam.c(), 'C')

        # test that we can parse DEFAULT
        letterParam.parse(DEFAULT)

        self.assertEqual(letterParam.a(), 'A')
        self.assertEqual(letterParam.b(), 'B')
        self.assertEqual(letterParam.c(), 'C')

        # test that we can parse a dict and leave defaults
        letterDict = {'b':'notb', 'c':'notc'}
        letterParam.parse(('letter', letterDict))

        self.assertEqual(letterParam.a(), 'A')
        self.assertEqual(letterParam.b(), letterDict['b'])
        self.assertEqual(letterParam.c(), letterDict['c'])

        # test that we can re-parse a param
        letterDict = {'b':'notbagain'}
        letterParam.parse(('letter', letterDict))

        self.assertEqual(letterParam.a(), 'A')
        self.assertEqual(letterParam.b(), letterDict['b'])
        
        # test that we get an exception when parsing a nonexistent
        # dict key
        letterDict = {'b':'B', 'bad':'BadKey'}
        self.assertRaises(ParamParseError, 
                          letterParam.parse, ('letter', letterDict))

        # test that we get an exception when missing a required parameter
        letterParam.b.importance = ParamImportance.REQUIRED
        # letterDict = {'a':'A'}
        letterDict = DEFAULT
        self.assertRaises(ParamParseError, letterParam.parse, letterDict)
        
    def test_ParentParamXMLParse(self):

        letterParam = self.getLetterParentParam()
        
        self.assertEqual(letterParam.a(), 'A')
        self.assertEqual(letterParam.b(), 'B')
        self.assertEqual(letterParam.c(), 'C')

        # test that we can parse an element tree and leave defaults
        letterEl = ET.Element('letters')
        letterEl.append(ET.Element('a', {'val': "'notA'"}))
        letterEl.append(ET.Element('c', {'val': "'notB'"}))
        letterParam.parse(letterEl)

        self.assertEqual(letterParam.a(), 
                         eval(letterEl.find('a').attrib['val']))
        self.assertEqual(letterParam.b(), 'B')
        self.assertEqual(letterParam.c(), 
                         eval(letterEl.find('c').attrib['val']))

        # test that we can re-parse a param
        letterEl.find('a').attrib['val'] = "'notaagain'"
        letterParam.parse(letterEl)

        self.assertEqual(letterParam.a(), 
                         eval(letterEl.find('a').attrib['val']))
        self.assertEqual(letterParam.b(), 'B')
        
        # test that we get an exception when parsing a nonexistent
        # element
        letterEl = ET.Element('letters')
        letterEl.append(ET.Element('a', {'val': "'A'"}))
        letterEl.append(ET.Element('badkey', {'val': "'BadKey'"}))
        self.assertRaises(ParamParseError, letterParam.parse, letterEl)

        # test that we get an exception when missing a required parameter
        letterParam.b.importance = ParamImportance.REQUIRED
        letterEl = ET.Element('letters')
        letterEl.append(ET.Element('a', {'val': "'A'"}))
        self.assertRaises(ParamParseError, letterParam.parse, letterEl)

    def test_ListParamParse(self):
        
        peopleParam = self.getPeopleListParam()

        # test defaults
        self.assertEqual(len(peopleParam), 2)
        self.assertEqual(peopleParam[0].name(), 'John Doe')
        self.assertEqual(peopleParam[1].name(), 'Adam Smith')

        # show that defaults are removed when we parse a value
        xml_text = \
            """<people>
        <person>
        <name val="%r" />
        <age val="%r" />
        </person>
        </people>"""%('Bill Clinton', 68)
        
        param = peopleParam.parseXMLString(xml_text)
        self.assertEqual(len(param), 1)
        self.assertEqual(param[0].name(), 'Bill Clinton')
        self.assertEqual(param[0].age(), 68)

        # show that defaults are updated, but don't update defaults
        # this time
        param.updateDefaultOnParse = False
        param = param.configuredCopy([{'person': {'age':21}}])
        self.assertEqual(len(param), 1)
        self.assertEqual(param[0].name(), 'Bill Clinton')
        self.assertEqual(param[0].age(), 21)

        # and now show that the template was not updated
        peopleEl = ET.Element('people')
        personEl = ET.Element('person')
        peopleEl.append(personEl)
        personEl.append(ET.Element('name', {'val': "'Happy Gilmore'"}))
        personEl.append(ET.Element('age', {'val': "35"}))
        personEl = ET.Element('person')
        peopleEl.append(personEl)
        personEl.append(ET.Element('name', {'val': "'Noam Chomsky'"}))
        param = param.configuredCopy(peopleEl)
        self.assertEqual(len(param), 2)
        self.assertEqual(param[0].name(), 'Happy Gilmore')
        self.assertEqual(param[0].age(), 35)
        self.assertEqual(param[1].name(), 'Noam Chomsky')
        self.assertEqual(param[1].age(), 68)

    def test_EnumParamParse(self):
        
        TestEnumParam = self.getNumberEnumParam()

        # test defaults
        self.assertEqual(TestEnumParam(), self.NumberEnum.One)

        # test parsing an xml string
        xml_text = \
            '<NumberEnum val="%r" />'%'Two'
        param = TestEnumParam.parseXMLString(xml_text)
        self.assertEqual(param(), self.NumberEnum.Two)

        # test parsing a yaml string
        yaml_text = \
            'NumberEnum : %s'%'Two'
        param = TestEnumParam.parseYAMLString(yaml_text)
        self.assertEqual(param(), self.NumberEnum.Two)

        # test xml/yaml output
        xml_text = param.toXML()
        self.assertRegexpMatches(xml_text, '''.*"'Two'".*''')

        yaml_text = param.toYAML()
        self.assertRegexpMatches(yaml_text, ".*Two$")

        # show that we get an exception if parsed value isn't a valid
        # enum value
        xml_text = \
            '<NumberEnum val="%r" />'%'EnumNotFound'
        self.assertRaises(ParamParseError, 
                          TestEnumParam.parseXMLString, 
                          xml_text)

    def test_XORParam(self):
        

        ParamOpt1 = ParentParam(
            name='ParamOpt1',
            children=[
                Param(name='P1OptA',
                      value='P1ValA',
                      comment='Param 1 Setting A'),
                Param(name='P1OptB',
                      value='P1ValB',
                      comment='Param 1 Setting B')],
            comment='One of three possible params')
                      

        ParamOpt2 = ParentParam(
            name='ParamOpt2',
            children=[
                Param(name='P2OptA',
                      value='P2ValA',
                      comment='Param 2 Setting A'),
                Param(name='P2OptB',
                      value='P2ValB',
                      comment='Param 2 Setting B')],
            comment='One of three possible params')
                      
        ParamOpt3 = ParentParam(
            name='ParamOpt3',
            children=[
                Param(name='P3OptA',
                      value='P3ValA',
                      comment='Param 3 Setting A'),
                Param(name='P3OptB',
                      value='P3ValB',
                      comment='Param 3 Setting B')],
            comment='One of three possible params')
                      
        TestXORParam = XORParam(name='XORTestParam', 
                                subparams=\
                                    [ParamOpt1,
                                     ParamOpt2,
                                     ParamOpt3],
                                comment='Test XOR Param')

        opt3dict = {'P3OptA': 'SomeNewVal'}
        opt3data = ('ParamOpt3', {'P3OptA': 'SomeNewVal'})
        xorp = TestXORParam.configuredCopy(opt3data)
        self.assertEqual(xorp.P3OptA(), opt3data[1]['P3OptA'])

        # add TestXORParam to ParentParam
        XORParent = ParentParam(
            name='XORParent',
            children = [TestXORParam],
            comment = 'parent param containing XORParam')
        
        parent_xorp_dict = {'ParamOpt3': opt3dict}
        parent_xorp = XORParent.configuredCopy(parent_xorp_dict)
        # can refer to parameter by XOR name or subparam name
        parent_xorp.ParamOpt3.P3OptA()
        self.assertEqual(parent_xorp.ParamOpt3.P3OptA(), opt3data[1]['P3OptA'])
        self.assertEqual(parent_xorp.XORTestParam.P3OptA(), opt3data[1]['P3OptA'])

    def test_toTreeDict(self):
        
        letterparam = self.getLetterXORParam()
        numberparam = self.getNumberEnumParam()
        peopleparam = self.getPeopleListParam()

        tdtest = \
            ParentParam('tdtest',
                        [letterparam,
                         numberparam,
                         peopleparam],
                        comment='treedict test param')

        tdtestdict = \
            {'tdtest': {'c': 'cat',
                        'NumberEnum': self.NumberEnum.Zero,
                        'people': [{'person': {'name': 'fooname1',
                                               'age': 1}},
                                   {'person': {'name': 'fooname2',
                                               'age': 2}},
                                   {'person': {'name': 'fooname3',
                                               'age': 3}}]
                        }
             }
                                   
        td = tdtest.toTreeDict()
        print td.makeReport()
        tdp = tdtest.configuredCopy(tdtestdict['tdtest'])
        td = tdp.toTreeDict(td)
        print td.makeReport()

        self.assertEqual(td.people[0].name,
                    tdtestdict['tdtest']['people'][0]['person']['name'])
        self.assertEqual(td.people[1].age, 
                    tdtestdict['tdtest']['people'][1]['person']['age'])
        self.assertEqual(td.NumberEnum, 0)
        self.assertEqual(td.LetterXORParam, 'cat')
        self.assertEqual(td.c, 'cat')
        # this is wrong, but works
        # self.assertEqual(td.a, 'cat')

    def test_doParseTD(self):
        
        lettersparam = self.getLettersParam()
        numberparam = self.getNumberEnumParam()
        teamparam = self.getTeamListParam()

        tdtest = \
            ParentParam('tdtest',
                        [lettersparam,
                         numberparam,
                         teamparam],
                        comment='treedict test param')


        t = TreeDict('tdtest')
        # Letters is a ListParam of XORParams
        t.Letters = NamedList('Letters')
        t.Letters.append(TreeDict('f'))
        t.Letters.append(TreeDict('e'))
        t.Letters.append(TreeDict('e'))
        t.Letters.append(TreeDict('d'))
        t.NumberEnum = self.NumberEnum.Zero
        # team is a ListParam of ListParams
        t.team = NamedList('team',
                           td=NamedList('people', td='person'),
                           nEntries=2)
        # team one
        t.team[0].init_back(3)
        t.team[0][0].name = 'fooname1'
        t.team[0][0].age = 1
        t.team[0][1].name = 'fooname2'
        t.team[0][1].age = 2
        t.team[0][2].name = 'fooname3'
        t.team[0][2].age = 3
        # team two
        t.team[1].init_back(2)
        t.team[1][0].name = 'Burt'
        t.team[1][0].age = 23
        t.team[1][1].name = 'Bart'
        t.team[1][1].age = 42
                                   
        tdp = tdtest.configuredCopy(t)

        self.assertEqual(tdp.team[0][0].name(),
                         t.team[0][0].name)
        self.assertEqual(tdp.team[1][1].age(),
                         t.team[1][1].age)
        self.assertEqual(tdp.NumberEnum(), 0)
        self.assertEqual(tdp.Letters[0].parsedName(), 'f')
        self.assertEqual(tdp.Letters[1].parsedName(), 'e')

    def runTest():
        pass

if __name__ == '__main__':

    # yaml test
    
    # bla = {'outer':{'somekey': 'someval',
    #                 'dictkey': {'innerkey':'innerval',
    #                             'ik2':'iv2'},
    #                 'anotherkey': 'anotherval',
    #                 'listKey': ['bla', 'foo', 'blarg'],
    #                 'dictListKey': [{'e1k1':'e1v1', 
    #                                  'e1k2':'e1v2', 
    #                                  'e1k3':'e1v3'},
    #                                 {'e2k1':'e2v1', 
    #                                  'e2k2':'e2v2', 
    #                                  'e2k3':'e2v3'},
    #                                 {'e3k1':'e3v1', 
    #                                  'e3k2':'e3v2', 
    #                                  'e3k3':'e3v3'}]}}
    
    # print yaml.dump(bla, default_flow_style=False, indent=4)

    # Param test
    
    camParamChildren = [
        Param(name='sz', value=[500,500], 
              comment='resolution of images to generate'),
        Param(name='fileNameBase', value='image',
              comment='filename to use for saved data'),
        Param(name='sensorHeight', value=4.0,
              comment='height of sensor in mm'),
        Param(name='focalLength', value=2.0,
              comment='focal length of camera in mm'),
        Param(name='camPos', value=[0.0, 0.0, 0.0],
              comment="position of 'pinhole' of camera"),
        Param(name='camDir', value=[0.0, 0.0, 1.0],
              comment='direction in which the camera points, '
              'for now should be along z-axis'),
        Param(name='camClipRange', value=[500, 10000.0],
              comment='Near and far clip plane distances in mm'),
        Param(name='dist', value=False,
              comment='If True, compute dist to camera pinhole. '
              ' If False, compute orthogonal dist to camera plane.')]
    
    CameraParam = \
        ParentParam(name='camera', \
                        children=camParamChildren, \
                        comment='Camera Configuration Parameters')
    
    letterChildren = [
        Param(name='a', value='A', 
              comment='the letter A'),
        Param(name='b', value='B', 
              comment='the letter B'),
        Param(name='c', value='C', 
              comment='the letter C')]

    LetterParam = \
        ParentParam(name='letter', \
                        children=letterChildren, \
                        comment='Letters of the Alphabet')

    planeParamChildren = [
        Param(name='planeCenter', value=[0.0, 0.0, 0.0],
              comment='center point of plane in world space'),
        Param(name='planeSz', value=1.0,
              comment='width/height of plane'),
        Param(name='planeTex', value=None,
              comment='texture file for plane')]

    PlaneParam = \
        ParentParam(name='plane', children=planeParamChildren, \
                        comment='settings for textured scene plane')


    basedir = '/home/jsam/Projects/StereoVision'

    planeList = [ 
        PlaneParam.configuredCopy(
            {'planeCenter':[-500.0, 0.0, 1000.0],
             'planeSz':500,
             'planeTex':basedir+'/Textures/masonry.png'}),
        PlaneParam.configuredCopy(
            {'planeCenter':[0.0, 0.0, 2000.0],
             'planeSz':500,
             'planeTex':basedir+'/Textures/masonry.png'}),
        PlaneParam.configuredCopy(
            {'planeCenter':[500.0, 0.0, 3000.0],
             'planeSz':500,
             'planeTex':basedir+'/Textures/masonry.png'}),
        PlaneParam.configuredCopy(
            {'planeCenter':[0.0, 0.0, 5000.0],
             'planeSz':5000,
             'planeTex':basedir+'/Textures/earth.png'})]
    
    PlaneListParam = \
        ListParam(name='planeList', value=planeList, \
                      comment='list of planes in scene')

    OptEnum = Enum.Enum('Option1','Option2','SomeOtherOption')
    OptParam = EnumParam(name='OptParam', enumType=OptEnum,
                         value=OptEnum.Option2,
                         comment='Enumerated Option Param')

    TtlParamChildren = [
        OptParam,
        CameraParam,
        LetterParam,
        PlaneListParam,
        Param(name='anotherParam', value='someval',
              comment='bla bla bla')]

    TtlParam = \
        ParentParam(name='TotalConfig', \
                        children=TtlParamChildren, \
                        comment='The main config')

    yaml_text = TtlParam.toYAML()
    print yaml_text
    with open('test.yaml','w') as f:
       f.write(yaml_text)

    paramYAML = TtlParam.parseParamFile('test.yaml')

    xml_text = paramYAML.toXML()
    print xml_text
    with open('test.xml','w') as f:
       f.write(xml_text)

    paramXML = TtlParam.parseParamFile('test.xml')

    
        
        

