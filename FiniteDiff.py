import PyCA.Core as core
import PyCA.Common as common

# 2D functions

def dxdx_tight(outIm, inIm):
    sp = inIm.spacing()
    mType = inIm.memType()
    stencilArr = np.zeros([3,1])
    stencilArr[0,0] = 1.0/sp.x
    stencilArr[1,0] = -2.0/sp.x
    stencilArr[2,0] = 1.0/sp.x
    stencil = common.ImFromNPArr(stencilArr, sp=sp, mType=mType)
    core.Convolve(outIm, inIm, stencil)

def dydy_tight(outIm, inIm):
    sp = inIm.spacing()
    stencilArr = np.zeros([1,3])
    stencilArr[0,0] = 1.0/sp.y
    stencilArr[0,1] = -2.0/sp.y
    stencilArr[0,2] = 1.0/sp.y
    stencil = common.ImFromNPArr(stencilArr, sp=sp, mType=mType)
    core.Convolve(outIm, inIm, stencil)
    
def dxdy(outIm, inIm):
    sp = inIm.spacing()
    stencilArr = np.zeros([3,3])
    stencilArr[0,0] = 1.0/sp.x
    stencilArr[2,2] = 1.0/sp.x
    stencilArr[0,2] = -1.0/sp.x
    stencilArr[2,0] = -1.0/sp.x
    stencil = common.ImFromNPArr(stencilArr, sp=sp, mType=mType)
    core.Convolve(outIm, inIm, stencil)
    
