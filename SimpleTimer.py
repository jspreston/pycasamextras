import time

class SimpleTimer(object):

    def __init__(self, name=None):
        self.start_time = time.time()
        self.stop_time = None
        self.name = name

    def finish(self):
        self.stop_time = time.time()
        return self

    def total_time(self):
        if self.stop_time is not None:
            return self.stop_time-self.start_time
        else:
            return self.elapsed_time()

    def elapsed_time(self):
        return time.time() - self.start_time

    def __str__(self):
        if self.name is None:
            s = ''
        else:
            s = self.name + ' '
        return s + 'took %f seconds'%self.total_time()
