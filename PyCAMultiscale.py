"""multiscale/multiresolution for PyCA images and fields

Defines the class MultiscaleManager, which handles
multiscale/multiresolution for PyCA images and fields

Multiscale input and result fields are created as subclasses of
Image3D and Field3D (wrapper classes InputImage, ResultImage, and
ResultField).  There are generated via calls to
e.g. MultiscaleManager.newInputImage(...).  When nextScale() is called
on the MultiscaleManager, all the associated images and fields are
automatically up/downsampled appropriately.

"""
import PyCA.Core as ca
import PyCA.Common as common

import numpy as np

from collections import Iterable
import itertools


def getSigFromDSFact(dsfact, scalespace_mode=False, is_slice=False):
    """
    If scalespace_mode is True, use more aggressive blurring
    """

    ndims = 2 if is_slice else 3

    if not isinstance(dsfact, Iterable):
        dsfact = [dsfact]*ndims

    dsfact = np.array(dsfact, dtype=float)

    if scalespace_mode:
        sig = dsfact
    else:
        sig = np.sqrt(dsfact/2.0)

    if is_slice:
        return ca.Vec3Df(sig[0], sig[1], 1.0)
    else:
        return ca.Vec3Df(sig[0], sig[1], sig[2])


def getKRadFromSig(sig, is_slice=False):

    assert isinstance(sig, ca.Vec3Df)

    krad = np.ceil(3*np.array(sig.tolist())).astype(int)

    if is_slice:
        krad = ca.Vec3Di(krad[0], krad[1], 0)
    else:
        krad = ca.Vec3Di(krad[0], krad[1], krad[2])
    return krad


def grid_from_sz(sz):
    """
    sz can be a grid, or Vec3Di or list specifying size
    """
    if isinstance(sz, ca.GridInfo):
        grid = sz
    elif isinstance(sz, ca.Vec3Di):
        grid = ca.GridInfo(sz)
    else:
        sz = list(sz)
        while len(sz) < 3:
            sz.append(1)
        grid = ca.GridInfo(ca.Vec3Di(sz[0], sz[1], sz[2]))
    return grid


def grid_from_dsfact(dsfact, orig_grid):

    orig_sz = np.array(orig_grid.size().tolist())
    orig_sp = np.array(orig_grid.spacing().tolist())

    sz = np.floor(orig_sz/dsfact).astype(int)
    sz = np.maximum(sz, 1)
    sp = orig_sp*(orig_sz/sz.astype(float))

    imsz = ca.Vec3Di(sz[0], sz[1], sz[2])
    imsp = ca.Vec3Df(sp[0], sp[1], sp[2])
    imor = orig_grid.origin()

    grid = ca.GridInfo(imsz, imsp, imor)
    return grid

def dsfact_is_one(dsfact):
    """
    Returns true if dsfact represents no downsampling

    this is true if dsfact is 1 or [1,1,...]
    """
    if not isinstance(dsfact, Iterable):
        return dsfact == 1
    else:
        return np.all(np.array(dsfact) == 1)

class InputImage(ca.ManagedImage3D):
    """
    Input images get downsampled from an original image at each scale level
    """

    def __init__(self, orig_im, label_im=False,
                 scalespace_mode=False, blur_only=False):
        super(InputImage, self).__init__(orig_im.grid(), orig_im.memType())
        ca.Copy(self, orig_im)
        self.orig_im = ca.ManagedImage3D(orig_im.grid(), orig_im.memType())
        ca.Copy(self.orig_im, orig_im)
        self.is_slice = (self.size().z == 1)
        self.label_im = label_im
        self.scalespace_mode = scalespace_mode
        self.blur_only = blur_only

    def blurred_orig_im(self, out, dsfact):

        if dsfact_is_one(dsfact):
            ca.Copy(out, self.orig_im)
            return

        sig = getSigFromDSFact(
            dsfact,
            scalespace_mode=self.scalespace_mode,
            is_slice=self.is_slice)
        krad = getKRadFromSig(sig, is_slice=self.is_slice)

        if self.memType() is ca.MEM_DEVICE:
            gauss_filt = ca.GaussianFilterGPU()
        else:
            gauss_filt = ca.GaussianFilterCPU()

        gauss_filt.updateParams(self.orig_im.size(), sig, krad)
        tmp_im = ca.ManagedImage3D(self.orig_im.grid(), self.memType())
        gauss_filt.filter(out, self.orig_im, tmp_im)

    def set_downsample(self, dsfact):

        if self.blur_only:
            if self.label_im or dsfact_is_one(dsfact):
                ca.Copy(self, self.orig_im)
            else:
                self.blurred_orig_im(self, dsfact)
        else:

            grid = grid_from_dsfact(dsfact, self.orig_im.grid())
            self.setGrid(grid)

            if dsfact_is_one(dsfact):
                ca.Copy(self, self.orig_im)
                return

            if self.label_im:
                # nearest-neighbor interpolation resample, using
                # origin offset (final True param)
                ca.Resample(self, self.orig_im,
                            ca.BACKGROUND_STRATEGY_CLAMP,
                            ca.INTERP_NN, True)
            else:
                blurred_im = \
                    ca.ManagedImage3D(self.orig_im.grid(),
                               self.memType())
                self.blurred_orig_im(blurred_im, dsfact)
                # resample, using origin offset (final True param)
                ca.Resample(self, blurred_im,
                            ca.BACKGROUND_STRATEGY_CLAMP,
                            ca.INTERP_LINEAR, True)


class ResultImage(ca.ManagedImage3D):
    """
    Result images get upsampled from previous results at each scale level
    """

    def __init__(self, grid, mType, blur_only=False, label_im=False):
        super(ResultImage, self).__init__(grid, mType)
        ca.SetMem(self, 0.0)
        self.blur_only = blur_only
        self.label_im = label_im

    def resample(self, newsz):

        if self.blur_only:
            return

        grid = grid_from_sz(newsz)

        out_im = ca.ManagedImage3D(grid, self.memType())
        if self.label_im:
            interp_type = ca.INTERP_NN
        else:
            interp_type = ca.INTERP_LINEAR
        # resample, using origin offset (final True param)
        ca.Resample(out_im, self, ca.BACKGROUND_STRATEGY_CLAMP,
                    interp_type, True)

        self.setGrid(grid)
        ca.Copy(self, out_im)


class ResultField(ca.ManagedField3D):
    """
    Result fields get upsampled from previous results at each scale level
    """

    def __init__(self, grid, mType, is_offset=True, blur_only=False):
        super(ResultField, self).__init__(grid, mType)
        self.is_offset = is_offset
        self.blur_only = blur_only
        if self.is_offset:
            ca.SetToZero(self)
        else:
            ca.SetToIdentity(self)

    def resample(self, newsz):

        if self.blur_only:
            return

        grid = grid_from_sz(newsz)

        out_field = ca.ManagedField3D(grid, self.memType())

        if not self.is_offset:
            ca.HtoV_I(self)

        # resample, not rescaling vectors (final False)
        ca.Resample(out_field, self,
                    ca.BACKGROUND_STRATEGY_CLAMP, False)

        if not self.is_offset:
            ca.VtoH_I(out_field)

        self.setGrid(grid)
        ca.Copy(self, out_field)


class MultiscaleManager(object):

    def __init__(self, scale, orig_grid, mType,
                 scalespace_mode=False, blur_only=False):
        """
        scale can be an integer number of scales, in which
        downsampling by two is used, or a list of downsample values
        """
        if isinstance(scale, Iterable):
            self.dsfact = list(reversed(sorted(scale)))
        else:
            self.dsfact = list(reversed(2**np.arange(scale)))

        self.orig_grid = orig_grid
        self.mType = mType
        self.scalespace_mode = scalespace_mode
        self.blur_only = blur_only
        self.is_slice = orig_grid.size().z == 1
        self.ndims = 2 if self.is_slice else 3

        self.InputImages = []
        self.ResultImages = []
        self.ResultFields = []

        self.cur_scale = -1

    def initialized(self):
        return self.cur_scale >= 0

    def nScales(self):
        return len(self.dsfact)

    def curScale(self):
        return self.cur_scale

    def _getDSFact(self, scale_idx):
        """ return length-3 downsample factor """
        assert 0 <= scale_idx < self.nScales()
        curds = self.dsfact[scale_idx]
        if isinstance(curds, Iterable):
            curds = list(curds)
            while len(curds) < 3:
                curds.append(1)
        else:
            curds = [curds]*3
        return curds

    def curDS(self):
        """return a 3-array giving the current downsample factors in each
        dimension (compared to the original data)

        """
        return self._getDSFact(self.cur_scale)

    def curDSFactVsPrev(self):
        """return a 3-array giving the upsampling factor (in each dimension)
        from the previous scale to the current.

        """
        if self.cur_scale == 0:
            # no previous scale
            return None

        cur = np.array(self._getDSFact(self.cur_scale)).astype(float)
        prev = np.array(self._getDSFact(self.cur_scale-1)).astype(float)

        return prev/cur

    def curGrid(self):

        if self.blur_only:
            return self.orig_grid.copy()
        else:
            dsfact = self.curDS()
            return grid_from_dsfact(dsfact, self.orig_grid)

    def newInputImage(self, orig_im, label_im=False):
        """
        See documentation for InputImage.__init__
        """

        assert not self.initialized()

        im = InputImage(orig_im, label_im=label_im,
                        scalespace_mode=self.scalespace_mode,
                        blur_only=self.blur_only)
        self.InputImages.append(im)
        return im

    def newResultImage(self, label_im=False,
                       init=None, rescale_init=False):
        """
        See documentation for ResultImage.__init__
        """

        assert not self.initialized()

        im = ResultImage(grid=self.orig_grid, mType=self.mType,
                         blur_only=self.blur_only, label_im=label_im)
        self.ResultImages.append(im)
        if init is not None:
            if im.grid() == init.grid():
                ca.Copy(im, init)
                init = None

        # add init image ref and info to ResultImage
        im.init = init
        im.rescale_init = rescale_init

        return im

    def newResultField(self, is_offset,
                       init=None, rescale_init=False):
        """
        See documentation for ResultField.__init__
        """

        assert not self.initialized()

        f = ResultField(grid=self.orig_grid, mType=self.mType,
                        is_offset=is_offset,
                        blur_only=self.blur_only)
        self.ResultFields.append(f)
        if init is not None:
            if f.grid() == init.grid():
                ca.Copy(f, init)
                init = None

        f.init = init
        f.rescale_init = rescale_init

        return f

    def nextScale(self):

        self.cur_scale += 1
        dsfact = self.curDS()
        grid = self.curGrid()

        for im in self.InputImages:
            im.set_downsample(dsfact)

        for im in self.ResultImages:
            im.resample(grid)

        for f in self.ResultFields:
            f.resample(grid)

        # handle any initialization at first scale level
        if self.cur_scale == 0:
            oblist = itertools.chain(self.ResultImages, self.ResultFields)

            for ob in oblist:
                if ob.init is not None:
                    if ob.grid() == ob.init.grid():
                        ca.Copy(ob, ob.init)
                    elif ob.rescale_init:
                        if isinstance(ob, ca.Image3D):
                            ca.Resample(ob, ob.init,
                                        ca.BACKGROUND_STRATEGY_CLAMP,
                                        ob.interp_type, True)
                        else:
                            ca.Resample(ob, ob.init,
                                        ca.BACKGROUND_STRATEGY_CLAMP,
                                        False)
                    else:
                        raise Exception('grids not equal and rescale_init is False')

                del ob.init
                del ob.rescale_init

if __name__ == '__main__':

    import matplotlib.pyplot as plt
    import PyCA.Display as display
    import PyCA.Numpy as canp

    # test image
    mType = ca.MEM_HOST

    blur_only = False
    n_scales = 4
    scalespace_mode = True

    I0Orig = common.LoadImage('/home/sci/jsam/data/LayeredDefData/Wire/Frames/Wire000.png', mType)
    origGrid = I0Orig.grid().copy()

    scaleManager = MultiscaleManager(scale=n_scales, orig_grid=origGrid,
                                     mType=mType,
                                     blur_only=blur_only,
                                     scalespace_mode=scalespace_mode)
    I0 = scaleManager.newInputImage(I0Orig)

    h = scaleManager.newResultField(is_offset=False)

    # Initialize the deformation at full size (create a rotation
    # field).  It will be downsampled on initial nextScale().
    harr = common.AsNPCopy(h)
    harr = canp.squeezeField(harr)
    sz = harr.shape[:-1]
    ndims = harr.shape[-1]
    eye = canp.identity(sz)
    # r = np.pi/4.0  # 45 degrees
    r = np.pi/18.0  # 10 degrees
    R = np.array([[np.cos(r), -np.sin(r)],
                  [np.sin(r), np.cos(r)]])
    eyemat = np.reshape(eye, (np.prod(sz), ndims)).T
    harr = R.dot(eyemat).T.reshape(sz+(ndims,))
    common.CopyDataFromNPArr(h, harr)

    # run up/downsampling at all scales
    nScales = scaleManager.nScales()
    plt.figure('downsample test')
    plt.clf()
    for scale in range(nScales):
        scaleManager.nextScale()

        curGrid = scaleManager.curGrid()
        defIm = ca.Image3D(curGrid, mType)
        ca.ApplyH(defIm, I0, h)

        plt.subplot(2, nScales, scale+1)
        display.DispImage(I0, 'scale %d' % scale, newFig=False)
        plt.subplot(2, nScales, nScales+scale+1)
        display.DispImage(defIm, 'def im, scale %d' % scale, newFig=False)
    plt.draw()
    plt.show()
