import Param

import PyCA.Core as ca
import PyCA.Common as common

import numpy as np

#
# Mask Parameter
#
NoMaskParam = \
    Param.Param(name='NoMask',
                value=None,
                comment='Do not use layer mask')

BorderMaskParam = \
    Param.ParentParam(
    name='BorderMask',
    children=[Param.Param(name='BorderPx',
                          value=8,
                          comment='Number of pixels to mask at border')])
CircleMaskParam = \
    Param.ParentParam(
    name='CircleMask',
    children=[Param.Param(name='BorderPx',
                          value=32,
                          comment='Minimum number of border pixels to mask, '
                          'circle rad = ImDim/2-BorderPix')])

MaskParam = Param.XORParam(name='Mask',
                           subparams=[NoMaskParam,
                                      BorderMaskParam,
                                      CircleMaskParam],
                           comment='Layer Mask Options')

def GenMask(param, sz,
            return_np=False,
            mType=ca.MEM_HOST,
            noMaskReturnsNone=True):
    """
    Return a mask of the given size via the specification in param
    """
    if param.parsedNameMatches('NoMask'):
        if noMaskReturnsNone:
            Mask = None
        else:
            Mask = np.ones(sz)
        print 'not using mask'
    elif param.parsedNameMatches('BorderMask'):
        Mask = np.zeros(sz)
        bdr = param.BorderPx()
        Mask[bdr:-bdr,bdr:-bdr] = 1.0
        print 'using border mask'
    elif param.parsedNameMatches('CircleMask'):
        bdr = param.BorderPx()
        Mask = common.DrawCircle(sz, \
                                 [dimsz/2 for dimsz in sz], \
                                 sz[0]/2-bdr)
        print 'using circle mask'
    else:
        raise Exception('Unknown mask type')
    
    if not return_np and Mask is not None:
        Mask = common.ImFromNPArr(Mask, mType)

    return Mask
    
