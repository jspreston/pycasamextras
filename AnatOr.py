
import PyCA.Core as core

import numpy as np

try:
    import matplotlib.pyplot as plt
    plt_loaded = True
except ImportError:
    print "Warning: matplotlib.pyplot not found, some functionality disabled"

import re

DIMMAP = {0:0,'x':0,1:1,'y':1,2:2,'z':2}
ormap = {'R':'RL','L':'RL','A':'AP','P':'AP','S':'SI','I':'SI'}

RAD_SLICE_AXIAL='RA'
RAD_SLICE_CORONAL='RS'
RAD_SLICE_SAGITTAL='PS'

# These functions take an orientation (e.g. 'RAI', indicating that
# (0,0,0) is most RAI point), and returns a slice where (0,0) is in
# the anatomical dispOr directions (ie if dispOr = 'RA' then (0,0) is
# the most 'RA' point of the slice, and the first dim increases in the
# L direction, the second in the P direction)
def ExtractOrientedSliceArrIm(npim,
                              orientation='RAI', dispOr='RA',
                              sliceIdx=None):
    
    # Make sure the orientation and dispOr make sense
    if None == re.match('[APSIRL]{3}$', orientation):
        raise Exception('Orientation seems invalid:  ' + orientation)
    if None == re.match('[APSIRL]{2}$', dispOr):
        raise Exception('display orientation seems invalid:  ' + dispOr)

    xSliceMinDir = dispOr[0]
    ySliceMinDir = dispOr[1]
    # get the axis not specified in dispOr (ie 'SI' for dispOr='RA')
    sliceAx = re.sub('[%s%s]'%(ormap[xSliceMinDir],ormap[ySliceMinDir]),'','APSIRL')

    # get the index of the slice dimension
    slicedim = re.search('[%s]'%sliceAx, orientation).start()
    m = re.search('[%s]'%ormap[xSliceMinDir], orientation)
    xVolMinDir = m.group()
    xRev = None
    if xVolMinDir != xSliceMinDir:
        xRev=-1
    xdim = m.start()
    m = re.search('[%s]'%ormap[ySliceMinDir], orientation)
    yVolMinDir = m.group()
    yRev = None
    if yVolMinDir != ySliceMinDir:
        yRev=-1
    ydim = m.start()

    if sliceIdx == None:
        sliceIdx = npim.shape[DIMMAP[slicedim]]/2
    
    idxarr = [0,0,0]
    idxarr[xdim] = slice(None,None,xRev)
    idxarr[ydim] = slice(None,None,yRev)
    idxarr[slicedim] = sliceIdx

    theslice = npim[idxarr[0], idxarr[1], idxarr[2]]
    if xdim > ydim:
        theslice = theslice.T

    return theslice


# Return a re-oriented version of the volume passed in
def ReorientVolArr(npim, inputOr='RAI', outputOr='RIA', returnMaps=False):
    
    # Make sure the orientation and dispOr make sense
    if None == re.match('[APSIRL]{3}$', inputOr):
        raise Exception('input orientation seems invalid:  ' + inputOr)
    if None == re.match('[APSIRL]{3}$', outputOr):
        raise Exception('output orientation seems invalid:  ' + outputOr)

    # dim stride will be -1 if the input dimension of the
    # corresponding index should be reversed, 1 otherwise
    dimStride = [1,1,1]
    # transposeMap will hold the transpose order -- ie [1,0,2] would
    # map input dimension 0 to output dimension 1, input dimension 1
    # to output dimension 0, and leave input dimension 2 as output
    # dimension 2
    transposeMap = [None,None,None]

    for dim in range(3):
        # get output dimension index corresponding to anatomical
        # direction for this dimension index in the input orientation
        inDimMinDir = inputOr[dim]
        m = re.search('[%s]'%ormap[inDimMinDir], outputOr)
        transposeMap[dim] = m.start()
        # determine if this dimension should be reversed
        outDimMinDir = m.group()
        if outDimMinDir != inDimMinDir:
            dimStride[dim] = -1

    out = npim[::dimStride[0],::dimStride[1],::dimStride[2]]
    out = np.transpose(out,transposeMap)
    
    if returnMaps:
        return (out, dimStride, transposeMap)
    else:
        return out

# Return a re-oriented version of the volume passed in
def ReorientVol(im, inputOr='RAI', outputOr='RIA'):

    origMemType = im.memType()
    im.toType(core.MEM_HOST)
    (outArr, dimStride, transposeMap) = \
        ReorientVolArr(npim=im.asnp(), inputOr=inputOr, \
                           outputOr=outputOr, returnMaps=True)
    oldGrid = im.grid()
    newSz = core.Vec3Di()
    newSp = core.Vec3Df()
    newOr = core.Vec3Df()
    for dim in range(3):
        newSz.set(transposeMap[dim], oldGrid.size().get(dim))
        newSp.set(transposeMap[dim], oldGrid.spacing().get(dim))
        newOr.set(transposeMap[dim], oldGrid.origin().get(dim))
    newGrid = core.GridInfo(newSz, newSp, newOr)
    out = core.Image3D(newGrid, core.MEM_HOST)
    out.asnp()[:,:,:] = np.ascontiguousarray(np.atleast_3d(outArr))

    im.toType(origMemType)
    out.toType(origMemType)

    return out
    
def SaveThreePaneArr(npim, outname, orientation='RAI', \
                         cmap='gray', rng=None):

    sz = npim.shape
    axSlice = ExtractOrientedSliceArrIm(npim, orientation, RAD_SLICE_AXIAL)
    coSlice = ExtractOrientedSliceArrIm(npim, orientation, RAD_SLICE_CORONAL)
    saSlice = ExtractOrientedSliceArrIm(npim, orientation, RAD_SLICE_SAGITTAL)
    # saSlice = ExtractOrientedSliceArrIm(npim, orientation, 'AS')
    sumx = axSlice.shape[0] + coSlice.shape[0] + saSlice.shape[0]
    maxy = max([axSlice.shape[1],coSlice.shape[1],saSlice.shape[1]])
    threePane = np.zeros([sumx,maxy])
    starty = (maxy-axSlice.shape[1])/2
    threePane[0:axSlice.shape[0],starty:starty+axSlice.shape[1]] = axSlice
    startx = axSlice.shape[0]
    starty = (maxy-coSlice.shape[1])/2
    threePane[startx:startx+coSlice.shape[0],starty:starty+coSlice.shape[1]] = coSlice
    startx = startx+coSlice.shape[0]
    starty = (maxy-saSlice.shape[1])/2
    threePane[startx:startx+saSlice.shape[0],starty:starty+saSlice.shape[1]] = saSlice
    
    if rng == None:
        vmin = None
        vmax = None
    else:
        vmin = rng[0]
        vmax = rng[1]

    plt.imsave(outname, \
                   threePane.T, cmap=cmap, \
                   vmin=vmin,vmax=vmax)

#
# R - one bright stripe
# L - one dark stripe
# A - two bright stripes
# P - two dark stripes
# S - three bright stripes
# I - three dark stripes
#
nOrStripeMap = {'R':1,'L':1,'A':2,'P':2,'S':3,'I':3}
stripeValMap = {'R':1,'L':-1,'A':1,'P':-1,'S':1,'I':-1}
def GenTestOrVol(orientation='RAI', sz=[64,64,64]):
    vol=np.zeros(sz)
    
    for dim,minDir in enumerate(orientation):

        for sIdx in range(nOrStripeMap[minDir]):
            idxarr = [slice(None),slice(None),slice(None)]
            idxarr[dim] = 2*sIdx+1

            vol[idxarr[0], idxarr[1], idxarr[2]] = stripeValMap[minDir];
            idxarr[dim] = -(2*sIdx+2)
            vol[idxarr[0], idxarr[1], idxarr[2]] = -stripeValMap[minDir];
    return vol
    
def TestOrientSlices(orientation='RAI', sz=[64,64,64]):
    vol = GenTestOrVol(orientation)

    center = (np.array(vol.shape)/2.0).astype('int')

    xline = vol[:,center[1],center[2]]
    yline = vol[center[0],:,center[2]]
    zline = vol[center[0],center[1],:]

    xSlice = vol[center[0],:,:]
    ySlice = vol[:,center[1],:]
    zSlice = vol[:,:,center[2]]

    aSlice = ExtractOrientedSliceArrIm(vol, orientation=orientation, \
                                                  dispOr=RAD_SLICE_AXIAL)
    cSlice = ExtractOrientedSliceArrIm(vol, orientation=orientation, \
                                                  dispOr=RAD_SLICE_CORONAL)
    sSlice = ExtractOrientedSliceArrIm(vol, orientation=orientation, \
                                                  dispOr=RAD_SLICE_SAGITTAL)

    plt.figure(1)
    plt.clf()

    plt.subplot(3,3,1)
    plt.plot(xline)
    plt.title('X')

    plt.subplot(3,3,2)
    plt.plot(yline)
    plt.title('Y')

    plt.subplot(3,3,3)
    plt.plot(zline)
    plt.title('Z')

    plt.subplot(3,3,4)
    plt.imshow(xSlice.T, cmap='gray')
    plt.title('X')

    plt.subplot(3,3,5)
    plt.imshow(ySlice.T, cmap='gray')
    plt.title('Y')

    plt.subplot(3,3,6)
    plt.imshow(zSlice.T, cmap='gray')
    plt.title('Z')

    plt.subplot(3,3,7)
    plt.imshow(aSlice.T, cmap='gray')
    plt.title('Axial')

    plt.subplot(3,3,8)
    plt.imshow(cSlice.T, cmap='gray')
    plt.title('Coronal')

    plt.subplot(3,3,9)
    plt.imshow(sSlice.T, cmap='gray')
    plt.title('Sagittal')

    plt.draw()
    plt.show()

def TestOrientVols(inputOr='LIP', outputOr='RAI', sz=[64,64,64]):
    inVol = GenTestOrVol(inputOr, sz)
    reqVol = GenTestOrVol(outputOr, sz)

    inXSlice = inVol[int(inVol.shape[0]/2),:,:]
    inYSlice = inVol[:,int(inVol.shape[1]/2),:]
    inZSlice = inVol[:,:,int(inVol.shape[2]/2)]

    reqXSlice = reqVol[int(reqVol.shape[0]/2),:,:]
    reqYSlice = reqVol[:,int(reqVol.shape[1]/2),:]
    reqZSlice = reqVol[:,:,int(reqVol.shape[2]/2)]

    outVol = ReorientVolArr(inVol, inputOr=inputOr, outputOr=outputOr)

    outXSlice = outVol[int(outVol.shape[0]/2),:,:]
    outYSlice = outVol[:,int(outVol.shape[1]/2),:]
    outZSlice = outVol[:,:,int(outVol.shape[2]/2)]

    plt.figure(1);
    plt.clf();

    plt.subplot(3,3,1);
    plt.imshow(inXSlice.T, cmap='gray')
    plt.title('Input X');

    plt.subplot(3,3,2);
    plt.imshow(inYSlice.T, cmap='gray')
    plt.title('Input Y');

    plt.subplot(3,3,3);
    plt.imshow(inZSlice.T, cmap='gray')
    plt.title('Input Z');

    plt.subplot(3,3,4);
    plt.imshow(reqXSlice.T, cmap='gray')
    plt.title('Requested X');

    plt.subplot(3,3,5);
    plt.imshow(reqYSlice.T, cmap='gray')
    plt.title('Requested Y');

    plt.subplot(3,3,6);
    plt.imshow(reqZSlice.T, cmap='gray')
    plt.title('Requested Z');

    plt.subplot(3,3,7);
    plt.imshow(outXSlice.T, cmap='gray')
    plt.title('Output X');

    plt.subplot(3,3,8);
    plt.imshow(outYSlice.T, cmap='gray')
    plt.title('Output Y');

    plt.subplot(3,3,9);
    plt.imshow(outZSlice.T, cmap='gray')
    plt.title('Output Z');

    plt.draw()
    plt.show()

